//
//  AppDelegate.m
//  Video2Photos
//
//  Created by Ashish Bhatt on 20/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "ELAudioManager.h"
#import "RateThisAppDialog.h"
#if SHOW_AD
    #import <RevMobAds/RevMobAds.h>
#endif
@implementation AppDelegate

@synthesize window = _window;
@synthesize navigationController = _navigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [[ELAudioManager sharedManager] playAppIntroductionSong];
//    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
#if SHOW_AD
    [RevMobAds startSessionWithAppID:REVMOB_ID];
#endif
    self.window.rootViewController = self.navigationController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    [RateThisAppDialog threeButtonLayoutWithTitle:@"Live Hot New Pic"
                                          message:@"Give it 5 Review!"
                                rateNowButtonText:@"YES!"
                              rateLaterButtonText:@"No"
                              rateNeverButtonText:nil];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
#if SHOW_AD
    [[RevMobAds session] showFullscreen];
#endif
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
//// YOU NEED TO CAPTURE igAPPID:// schema
//- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
//}

@end
