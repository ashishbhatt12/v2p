//
//  Video.m
//  iFrameExtractor
//
//  Created by lajos on 1/10/10.
//  Copyright 2010 www.codza.com. All rights reserved.
//

#import "VideoFrameExtractor.h"
#import "Utilities.h"
int64_t DesiredFrameNumber;
@interface VideoFrameExtractor (private)
-(void)convertFrameToRGB;
-(UIImage *)imageFromAVPicture:(AVPicture)pict width:(int)width height:(int)height;
-(void)savePicture:(AVPicture)pFrame width:(int)width height:(int)height index:(int)iFrame;
-(void)setupScaler;
@end

@implementation VideoFrameExtractor

@synthesize outputWidth, outputHeight,dtsValue;

-(void)setOutputWidth:(int)newValue {
	if (outputWidth == newValue) return;
	outputWidth = newValue;
	[self setupScaler];
}

-(void)setOutputHeight:(int)newValue {
	if (outputHeight == newValue) return;
	outputHeight = newValue;
	[self setupScaler];
}

-(UIImage *)currentImage {
	if (!pFrame->data[0]) return nil;
	[self convertFrameToRGB];
	return [self imageFromAVPicture:picture width:outputWidth height:outputHeight];
}

-(double)duration {
	return (double)pFormatCtx->duration / AV_TIME_BASE;
}

-(double)currentTime {
    AVRational timeBase = pFormatCtx->streams[videoStream]->time_base;
    return packet.pts * (double)timeBase.num / timeBase.den;
}

-(int)sourceWidth {
	return pCodecCtx->width;
}

-(int)sourceHeight {
	return pCodecCtx->height;
}

-(id)initWithVideo:(NSString *)moviePath {
	if (!(self=[super init])) return nil;
 
    AVCodec         *pCodec;
		
    // Register all formats and codecs
    avcodec_register_all();
    av_register_all();
	
    // Open video file
    if(avformat_open_input(&pFormatCtx, [moviePath cStringUsingEncoding:NSASCIIStringEncoding], NULL, NULL) != 0) {
        av_log(NULL, AV_LOG_ERROR, "Couldn't open file\n");
        goto initError;
    }
//    pFormatCtx->flags = AVFMT_SEEK_TO_PTS;
	
    // Retrieve stream information
    if(avformat_find_stream_info(pFormatCtx,NULL) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Couldn't find stream information\n");
        goto initError;
    }
    
    // Find the first video stream
    if ((videoStream =  av_find_best_stream(pFormatCtx, AVMEDIA_TYPE_VIDEO, -1, -1, &pCodec, 0)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot find a video stream in the input file\n");
        goto initError;
    }
	
    // Get a pointer to the codec context for the video stream
    pCodecCtx = pFormatCtx->streams[videoStream]->codec;
    
    // Find the decoder for the video stream
    pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
    if(pCodec == NULL) {
        av_log(NULL, AV_LOG_ERROR, "Unsupported codec!\n");
        goto initError;
    }
	
    // Open codec
    if(avcodec_open2(pCodecCtx, pCodec, NULL) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
        goto initError;
    }
	
    // Allocate video frame
    pFrame = avcodec_alloc_frame();
			
	outputWidth = pCodecCtx->width;
	self.outputHeight = pCodecCtx->height;
    
//    NSLog(@"Duration : %f || No. Of frames : %lld || AVG. Framerate : %d",[self duration],
//          pFormatCtx->streams[videoStream]->nb_frames,
//          pFormatCtx->streams[videoStream]->avg_frame_rate.num);
			
//    [self countFrames];
	return self;
	
initError:
	[self release];
	return nil;
}
- (int)countFrames
{
	// Get the total number of frames.
	// This is sometimes needed when the container is not giving us a reliable duration.
	// The only way to compute a solid duration is to parse all the frames and divide by framerate.
    
	int	iReadFrameResult;
	int iTotalFrames = 0;
	bool done = false;
    
	NSLog(@"Start scanning for frame count");
    
	do
	{
		// Read next packet
		AVPacket	InputPacket;
		iReadFrameResult = av_read_frame( pFormatCtx, &InputPacket);
        
		if(iReadFrameResult >= 0)
		{
			// Is this a packet from the video stream ?
			if(InputPacket.stream_index == videoStream)
			{
				iTotalFrames++;
				if(iTotalFrames % 1000 == 0)
				{
					NSLog(@"Scanning: {%d} frames.", iTotalFrames);
				}
				NSLog(@"Scanning: FRAME:[{%d}], DTS:[{%lld}], PTS:[{%lld}]",iTotalFrames, InputPacket.dts, InputPacket.pts);
			}
			
			// Free the packet that was allocated by av_read_frame
			av_free_packet(&InputPacket);
		}
		else
		{
			// Reading error. We don't know if the error happened on a video frame or audio one.
			done = true;
		}
	}
	while(!done);
    
	NSLog(@"End scanning for frame count: {%d}", iTotalFrames);
    
	return iTotalFrames;
}

-(void)setupScaler {

	// Release old picture and scaler
	avpicture_free(&picture);
	sws_freeContext(img_convert_ctx);	
	
	// Allocate RGB picture
	avpicture_alloc(&picture, PIX_FMT_RGB24, outputWidth, outputHeight);
	
	// Setup scaler
	static int sws_flags =  SWS_FAST_BILINEAR;
	img_convert_ctx = sws_getContext(pCodecCtx->width, 
									 pCodecCtx->height,
									 pCodecCtx->pix_fmt,
									 outputWidth, 
									 outputHeight,
									 PIX_FMT_RGB24,
									 sws_flags, NULL, NULL, NULL);
	
}
- (double)frameRate {
    AVRational r = pFormatCtx->streams[videoStream]->r_frame_rate;
    return (double)r.num/r.den;
}
- (void)seekAtFrame:(int)inFrame {
   long timestamp = (long)(1000000*inFrame/[self frameRate]);
    if (pFormatCtx == nil || pCodecCtx == nil) {
        timestamp = 0;
    } else {
        timestamp = timestamp * AV_TIME_BASE / 1000000;
        /* add the stream start time */
        if (pFormatCtx->start_time != AV_NOPTS_VALUE) {
            timestamp += pFormatCtx->start_time;
        }
        if(inFrame == 0)
            inFrame = 1;
        int64_t timestamp1 = (inFrame - 1) * dtsValue;
        NSLog(@"Seeking to timestamp : %lld",timestamp1);
        if (avformat_seek_file(pFormatCtx, videoStream, timestamp1,timestamp1, timestamp1, AVSEEK_FLAG_BACKWARD) < 0) {
            NSLog(@"Could not seek file to timestamp %lld" ,timestamp1 );
        }
        avcodec_flush_buffers(pCodecCtx);
    }
    return;
    av_seek_frame(pFormatCtx,videoStream,(inFrame - 1) * 20,0);
	avcodec_flush_buffers(pCodecCtx);    
}

- (void)seekTime:(double)seconds {
//        int av_seek_frame(AVFormatContext *s, int stream_index, int64_t timestamp,int flags);
//        int avformat_seek_file(AVFormatContext *s, int stream_index, int64_t min_ts, int64_t ts, int64_t max_ts, int flags);
//    [self seekMs:seconds];
//    return;
	AVRational timeBase = pFormatCtx->streams[videoStream]->time_base;
	int64_t targetFrame = (int64_t)((double)timeBase.den / timeBase.num * seconds);
//    NSLog(@"Targeted frame : %lld",targetFrame);
	avformat_seek_file(pFormatCtx, videoStream, targetFrame, targetFrame, targetFrame, AVSEEK_FLAG_FRAME);
//    avformat_seek_file(pFormatCtx, videoStream, INT64_MIN, targetFrame,INT64_MAX, AVSEEK_FLAG_FRAME);
//    av_seek_frame(pFormatCtx,videoStream,targetFrame,AVSEEK_FLAG_ANY);
//    av_seek_frame(pFormatCtx, -1, seconds * pFormatCtx->duration, 0) ;
	avcodec_flush_buffers(pCodecCtx);
}
- (BOOL)seekMs:(double)seconds
{
    //printf("**** SEEK TO ms %d. LLT: %d. LT: %d. LLF: %d. LF: %d. LastFrameOk: %d\n",tsms,LastLastFrameTime,LastFrameTime,LastLastFrameNumber,LastFrameNumber,(int)LastFrameOk);
    
    // Convert time into frame number
    DesiredFrameNumber = av_rescale(seconds / 1000,
                                            pFormatCtx->streams[videoStream]->time_base.den,
                                            pFormatCtx->streams[videoStream]->time_base.num);
    DesiredFrameNumber/=1000;
//    NSLog(@"Targeted new frame : %lld",DesiredFrameNumber);
    
    return [self seekFrame:DesiredFrameNumber];
}
- (BOOL)seekFrame:(int64_t) frame
{
    NSLog(@"Targeted frame : %lld",frame);

    //printf("**** seekFrame to %d. LLT: %d. LT: %d. LLF: %d. LF: %d. LastFrameOk: %d\n",(int)frame,LastLastFrameTime,LastFrameTime,LastLastFrameNumber,LastFrameNumber,(int)LastFrameOk);
    
    // Seek if:
    // - we don't know where we are (Ok=false)
    // - we know where we are but:
    //    - the desired frame is after the last decoded frame (this could be optimized: if the distance is small, calling decodeSeekFrame may be faster than seeking from the last key frame)
    //    - the desired frame is smaller or equal than the previous to the last decoded frame. Equal because if frame==LastLastFrameNumber we don't want the LastFrame, but the one before->we need to seek there
//    if( (LastFrameOk==false) || ((LastFrameOk==true) && (frame<=LastLastFrameNumber || frame>LastFrameNumber) ) )
    {
        //printf("\t avformat_seek_file\n");
        if(avformat_seek_file(pFormatCtx,videoStream,0,frame,frame,AVSEEK_FLAG_FRAME)<0)
            return false;
        
        avcodec_flush_buffers(pCodecCtx);
        
        DesiredFrameNumber = frame;
//        LastFrameOk=false;
    }
    //printf("\t decodeSeekFrame\n");
    
//    return decodeSeekFrame(frame);
    
    return true;
}

-(void)dealloc {
	// Free scaler
	sws_freeContext(img_convert_ctx);	

	// Free RGB picture
	avpicture_free(&picture);
    
    // Free the packet that was allocated by av_read_frame
    av_free_packet(&packet);
	
    // Free the YUV frame
    av_free(pFrame);
	
    // Close the codec
    if (pCodecCtx) avcodec_close(pCodecCtx);
	
    // Close the video file
    if (pFormatCtx) avformat_close_input(&pFormatCtx);
	
	[super dealloc];
}

-(BOOL)stepFrame {
	// AVPacket packet;
    int frameFinished=0;

    while(!frameFinished && av_read_frame(pFormatCtx, &packet)>=0) {
        // Is this a packet from the video stream?
        if(packet.stream_index==videoStream) {
            // Decode video frame
            if(dtsValue == 0)
                dtsValue = packet.dts - dtsValue;
            avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);
        }
	}
	return frameFinished!=0;
}

-(void)convertFrameToRGB {	
	sws_scale (img_convert_ctx, pFrame->data, pFrame->linesize,
			   0, pCodecCtx->height,
			   picture.data, picture.linesize);	
}

-(UIImage *)imageFromAVPicture:(AVPicture)pict width:(int)width height:(int)height {
	CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
	CFDataRef data = CFDataCreateWithBytesNoCopy(kCFAllocatorDefault, pict.data[0], pict.linesize[0]*height,kCFAllocatorNull);
	CGDataProviderRef provider = CGDataProviderCreateWithCFData(data);
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGImageRef cgImage = CGImageCreate(width, 
									   height, 
									   8, 
									   24, 
									   pict.linesize[0], 
									   colorSpace, 
									   bitmapInfo, 
									   provider, 
									   NULL, 
									   NO, 
									   kCGRenderingIntentDefault);
	CGColorSpaceRelease(colorSpace);
	UIImage *image = [UIImage imageWithCGImage:cgImage];
	CGImageRelease(cgImage);
	CGDataProviderRelease(provider);
	CFRelease(data);
	
	return image;
}

-(void)savePPMPicture:(AVPicture)pict width:(int)width height:(int)height index:(int)iFrame {
    FILE *pFile;
	NSString *fileName;
    int  y;
	
	fileName = [Utilities documentsPath:[NSString stringWithFormat:@"image%04d.ppm",iFrame]];
    // Open file
    NSLog(@"write image file: %@",fileName);
    pFile=fopen([fileName cStringUsingEncoding:NSASCIIStringEncoding], "wb");
    if(pFile==NULL)
        return;
	
    // Write header
    fprintf(pFile, "P6\n%d %d\n255\n", width, height);
	
    // Write pixel data
    for(y=0; y<height; y++)
        fwrite(pict.data[0]+y*pict.linesize[0], 1, width*3, pFile);
	
    // Close file
    fclose(pFile);
}

@end
