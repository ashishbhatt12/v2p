//
//  MovieDecoder.h
//  Video2Photos
//
//  Created by Ashish on 01/01/13.
//
//

#import <Foundation/Foundation.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <inttypes.h>
#include <string.h>
#include <vector.h>
#import "videoframe.h"
#include <libavutil/pixfmt.h>

//public:
//MovieDecoder(const std::string& filename, AVFormatContext* pavContext = NULL);
//~MovieDecoder();
//
//std::string getCodec();
//void seek(int timeInSeconds);
//void decodeVideoFrame();
//void getScaledVideoFrame(int scaledSize, bool maintainAspectRatio, VideoFrame& videoFrame);
//
//int getWidth();
//int getHeight();
//int getDuration();
//
//void initialize(const std::string& filename);
//void destroy();
//
//private:
//void initializeVideo();
//
//bool decodeVideoPacket();
//bool getVideoPacket();
//void convertAndScaleFrame(PixelFormat format, int scaledSize, bool maintainAspectRatio, int& scaledWidth, int& scaledHeight);
//void createAVFrame(AVFrame** pAvFrame, uint8_t** pFrameBuffer, int width, int height, PixelFormat format);
//void calculateDimensions(int squareSize, bool maintainAspectRatio, int& destWidth, int& destHeight);
//
//private:
//int                     m_VideoStream;
//AVFormatContext*        m_pFormatContext;
//AVCodecContext*         m_pVideoCodecContext;
//AVCodec*                m_pVideoCodec;
//AVStream*               m_pVideoStream;
//AVFrame*                m_pFrame;
//uint8_t*                m_pFrameBuffer;
//AVPacket*               m_pPacket;
//bool                    m_FormatContextWasGiven;
//bool                    m_AllowSeek;


@interface MovieDecoder : NSObject {
    @private
        int                     m_VideoStream;
        AVFormatContext*        m_pFormatContext;
        AVCodecContext*         m_pVideoCodecContext;
        AVCodec*                m_pVideoCodec;
        AVStream*               m_pVideoStream;
        AVFrame*                m_pFrame;
        uint8_t*                m_pFrameBuffer;
        AVPacket*               m_pPacket;
        bool                    m_FormatContextWasGiven;
        bool                    m_AllowSeek;
}
- (id)initWithFile:(const std::string&)filename context:(AVFormatContext*)pavContext;
- (std::string)getCodec;
- (void)seek:(int)timeInSeconds;
- (void)decodeVideoFrame;
- (void)getScaledVideoFrame:(int)scaledSize aspectRatio:(bool)maintainAspectRatio frame:(VideoFrame&)videoFrame;

- (int)getWidth;
- (int)getHeight;
- (int)getDuration;

- (void)initialize:(const std::string&)filename;
- (void)destroy;

@end
