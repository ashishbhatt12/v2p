//
//  FrameWriteOperation.h
//  iFrameExtractor
//
//  Created by Ashish Bhatt on 9/14/12.
//  Copyright (c) 2012 Capgemini. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol FrameWriteDelegate;
@interface FrameWriteOperation : NSOperation {
    @private
    NSInteger frameIndex;
    UIImage *frameImage;
    NSInteger videoID;
    NSString *folderName;
}
@property(nonatomic,strong)NSString *folderName;
@property(nonatomic,assign)NSObject<FrameWriteDelegate> *delegate;
@property(nonatomic,assign)NSInteger videoID;
@property(nonatomic,strong)UIImage *frameImage;
@property(nonatomic,assign)NSInteger frameIndex;
- (id)initWithImage:(UIImage *)inImage withInddex:(NSInteger)inIndex;
@end

@protocol FrameWriteDelegate
- (void)operation:(FrameWriteOperation *)inOperation didFinishWritingAtIndex:(NSInteger)inIndex;
@end