//
//  FrameWriteOperation.m
//  iFrameExtractor
//
//  Created by Ashish Bhatt on 9/14/12.
//  Copyright (c) 2012 Capgemini. All rights reserved.
//

#import "FrameWriteOperation.h"
#import "RIUtilities.h"
#import "Utilities.h"
@implementation FrameWriteOperation
@synthesize frameIndex;
@synthesize frameImage;
@synthesize videoID;
@synthesize delegate;
@synthesize folderName;
- (id)initWithImage:(UIImage *)inImage withInddex:(NSInteger)inIndex {
    if(self = [super init]) {
        self.frameIndex = inIndex;
        self.frameImage = inImage;
    }
    return self;
}
- (void)dealloc {
//    [super dealloc];
    self.delegate = nil;
}
- (void)main {
    @autoreleasepool {
//        NSString *frameFilePath = [RIUtilities outputImageFrameIndex:self.frameIndex];
        if([self isCancelled])
            return;
        NSString *frameFilePath = [RIUtilities framePathAtIndex:self.frameIndex forFolder:self.folderName];
        if([self isCancelled])
            return;
        NSData * imageData = UIImagePNGRepresentation(self.frameImage);
        if([self isCancelled])
            return;
        [imageData writeToFile:frameFilePath atomically:YES];
//        [[NSFileManager defaultManager] createFileAtPath:frameFilePath contents:imageData attributes:nil];
        if([self isCancelled])
            return;
        if([self.delegate respondsToSelector:@selector(operation:didFinishWritingAtIndex:)]) {
            [self.delegate operation:self didFinishWritingAtIndex:self.frameIndex];
        }
    }
}
@end
