//
//  AppDelegate.h
//  Video2Photos
//
//  Created by Ashish Bhatt on 20/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong,nonatomic)IBOutlet UIWindow *window;
@property (strong,nonatomic)IBOutlet UINavigationController *navigationController;
@end
