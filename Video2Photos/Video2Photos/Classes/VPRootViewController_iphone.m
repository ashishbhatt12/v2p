//
//  VPRootViewController_iphone.m
//  Video2Photos
//
//  Created by Ashish Bhatt on 20/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VPRootViewController_iphone.h"
#import "VPPhotoViewController.h"
#import "VPFrameExtractController.h"
#import "VPFrameListViewController_iphone.h"
#import "RIUtilities.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface VPRootViewController_iphone ()
@property(nonatomic,strong)AVAsset *videoAsset;
@end

@implementation VPRootViewController_iphone
@synthesize carousel;
@synthesize imageGenerator;
@synthesize videoAsset;
@synthesize activityView;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (VPImageGenerateHandler *)imageGeneratorHandlerForAssetURL:(NSURL *)inURL {
    if(!self.imageGenerator) {
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:inURL options:nil];
        self.videoAsset = asset;
        self.imageGenerator = [[VPImageGenerateHandler alloc] initWithAsset:asset];
        self.imageGenerator.delegate = self;
    }
    return self.imageGenerator;
}
- (VPImageGenerateHandler *)imageGeneratorHandler {
    if(!self.imageGenerator) {
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"Sydney-iPhone" ofType:@"m4v"];
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:videoPath] options:nil];
        self.videoAsset = asset;
        self.imageGenerator = [[VPImageGenerateHandler alloc] initWithAsset:asset];
        self.imageGenerator.delegate = self;
    }
    return self.imageGenerator;
}
- (IBAction)onCamera:(id)inSender {
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
        pickerController.delegate = self;
        pickerController.videoQuality = UIImagePickerControllerQualityType640x480;
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        pickerController.showsCameraControls = YES;
        pickerController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        pickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;        
        pickerController.videoMaximumDuration = 20.0f;
        [self presentModalViewController:pickerController animated:YES];
    } else {

        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"sophie" ofType:@"mov"];
        NSURL *videoURL = [NSURL fileURLWithPath:filePath];

        VPFrameExtractController *frameExtractor = [[VPFrameExtractController alloc] initWithVideoURL:videoURL];
        [frameExtractor startFrameExtraction];
        return;
        VPImageGenerateHandler *handler = [self imageGeneratorHandlerForAssetURL:videoURL];
        [self.carousel reloadData];
        return;
        
        UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
        pickerController.delegate = self;
//        pickerController.videoQuality = UIImagePickerControllerQualityType640x480;
        pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        pickerController.showsCameraControls = YES;
//        pickerController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
//        pickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;        
//        pickerController.videoMaximumDuration = 20.0f;
        [self presentModalViewController:pickerController animated:YES];
    }
}
// MARK: UIImagePickerControllerDelegates
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissModalViewControllerAnimated:NO];
    if([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.movie"] ) {

        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        VPImageGenerateHandler *handler = [self imageGeneratorHandlerForAssetURL:videoURL];
        [self.carousel reloadData];
//        [handler startImageGenerator];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];

    
    
    self.carousel.type = iCarouselTypeLinear;
    self.carousel.vertical = YES;
}
- (void)onSave:(id)inSender {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"sophie" ofType:@"mov"];
    NSURL *videoURL = [NSURL fileURLWithPath:filePath];

    if([RIUtilities isIphoneDevice]) {
        VPFrameListViewController_iphone *frameListController = [[VPFrameListViewController_iphone alloc] init];
        frameListController.videoURL = videoURL;
        [self.navigationController pushViewController:frameListController animated:YES];
    }
    [[self imageGenerator] startImageGenerator];
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //generate 100 buttons
    //normally we'd use a backing array
    //as shown in the basic iOS example
    //but for this example we haven't bothered
    return self.imageGenerator.totalNumberOfFrames;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{ 
    /*
    UIView *containerView  = (UIView *)view;
    if(!containerView) {
        //no button available to recycle, so create new one
		UIImage *image = [UIImage imageNamed:@"page.png"];
        containerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 250.0f, 200.0f)];
        containerView.opaque = NO;

        containerView.multipleTouchEnabled = NO;
        PosterImageView *posterView = [[PosterImageView alloc] init];
        posterView.frame = CGRectMake(0.0f, 0.0f, 250.0f, 200.0f);
        posterView.image = image;
        posterView.delegate = self;
        [containerView addSubview:posterView];
    }
    return containerView;
    */ 
	UIButton *button = (UIButton *)view;
	if (button == nil)
	{
		//no button available to recycle, so create new one
		UIImage *image = [UIImage imageNamed:@"page.png"];
		button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = index;
		button.frame = CGRectMake(0.0f, 0.0f, 250.0f, 200.0f);
		[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
		[button setBackgroundImage:image forState:UIControlStateNormal];
//        [button setImage:image forState:UIControlStateNormal];        
		button.titleLabel.font = [button.titleLabel.font fontWithSize:50];
        button.layer.borderColor = [UIColor whiteColor].CGColor;
//        button.layer.borderWidth = 1.0f;
//        button.layer.cornerRadius = 8.0f;
		[button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
	}
    [self.imageGenerator  generateImageForIndex:index];	    
	//set button label
	[button setTitle:[NSString stringWithFormat:@"%i", index] forState:UIControlStateNormal];
	
	return button;
}
#pragma mark -
#pragma PosterimageView delegate
- (void)posterImageViewWasSelected:(PosterImageView *)posterImageView {
    NSLog(@"%d",posterImageView.posterIndex);
}
#pragma mark -
#pragma mark Button tap event

- (void)buttonTapped:(UIButton *)sender {
    /*
    VPPhotoViewController *photoVideoController = [[VPPhotoViewController alloc] initWithNibName:@"VPPhotoViewController"
                                                                                          bundle:[NSBundle mainBundle]];
    photoVideoController.videoAsset = self.videoAsset;
    photoVideoController.photoIndex = [sender tag];
    [self.navigationController pushViewController:photoVideoController animated:YES];
    */
}
#pragma mark -
#pragma mark Image generator delegate
- (void)handler:(VPImageGenerateHandler *)inHandler didFindImage:(UIImage *)inImage forIndex:(NSInteger)inIndex {
    UIButton *button = (UIButton *) [carousel itemViewAtIndex:inIndex];
    [button setBackgroundImage:inImage forState:UIControlStateNormal];
    [carousel setNeedsDisplay];
}
- (void)didStartGeneratingImageSequence:(VPImageGenerateHandler *)inHandler {
    [self.activityView startAnimating];
    [self.carousel setUserInteractionEnabled:NO];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
}
- (void)didFinishGeneratingImageSequence:(VPImageGenerateHandler *)inHandler {
    [self.activityView stopAnimating];
    [self.carousel setUserInteractionEnabled:YES];    
    [self.navigationItem.rightBarButtonItem setEnabled:YES];    
}
- (void)handler:(VPImageGenerateHandler *)inHandler didFindImage:(UIImage *)inImage {
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeImageToSavedPhotosAlbum:[inImage CGImage] 
                              orientation:(ALAssetOrientation)[inImage imageOrientation]
                          completionBlock:^(NSURL *assetURL, NSError *error){
                              if (error) {
                                  // TODO: error handling
                              } else {
                                  // TODO: success handling
                              }
                          }];    
}
#pragma mark - VPFrameExtractDelegate
- (void)didStartExtractingFramesWithController:(VPFrameExtractController *)inController {
    
}
- (void)didFinishExtractingFramesWithController:(VPFrameExtractController *)inController {
    
}


@end
