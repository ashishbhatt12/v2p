//
//  VPRootViewController_ipad.h
//  Video2Photos
//
//  Created by Ashish Bhatt on 20/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VPRootViewController.h"
#import "VPImageGenerateHandler.h"
#import "iCarousel.h"
@interface VPRootViewController_ipad : VPRootViewController <iCarouselDataSource,iCarouselDelegate,VPImageGeneratorDelegate>{
    
}

@end
