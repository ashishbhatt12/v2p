//
//  VPPhotoViewController_ipad.h
//  Video2Photos
//
//  Created by Ashish on 07/11/12.
//
//

#import <UIKit/UIKit.h>
#import "VPFrameSaveOperation.h"
@interface VPPhotoViewController_ipad : UIViewController <VPFrameSaveDelegate> {
    @private    
    NSInteger photoIndex;
    NSString *frameFilePath;
    NSString *folderName;
}
@property(nonatomic,strong)NSString *folderName;
@property(nonatomic,strong)NSString *frameFilePath;
@property(nonatomic,assign)NSInteger photoIndex;
@property(nonatomic,weak)IBOutlet UIImageView *posterImageView;
@property(nonatomic,strong)UIImage *selectedImage;

@end
