//
//  VPPhotoViewController_ipad.m
//  Video2Photos
//
//  Created by Ashish on 07/11/12.
//
//

#import "VPPhotoViewController_ipad.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MessageUI/MessageUI.h>
#import "RIUtilities.h"
#import "UIImage+RoundedCorner.h"
#import "UIImage+Resize.h"
#import "ELSNSManager.h"

@interface VPPhotoViewController_ipad ()
@property(nonatomic,weak)IBOutlet UITableView *tableListView;
@property(nonatomic,assign)NSInteger numberOfFrames;
@property(nonatomic,weak)IBOutlet UIProgressView *progressBar;
@property(nonatomic,weak)IBOutlet UIToolbar *toolBar;
@property(nonatomic,strong)NSOperationQueue *frameSaveQueue;
- (void)saveToPhotoLibraryWithIndex:(NSInteger)inIndex;
- (void)saveAllPhotosToLibrary;
@end

@implementation VPPhotoViewController_ipad
@synthesize posterImageView;
@synthesize photoIndex;
@synthesize frameFilePath;
@synthesize frameSaveQueue;
@synthesize progressBar;
@synthesize toolBar;
@synthesize numberOfFrames;
@synthesize tableListView = _tableListView;
@synthesize folderName = _folderName;
@synthesize selectedImage = _selectedImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Photo export";
    
    [self loadImage];

}
- (void)loadImage {
    UIImage *frameImage = [[UIImage alloc] initWithContentsOfFile:self.frameFilePath];
    UIImage *thumbnail = [frameImage thumbnailImage:425
                                  transparentBorder:0
                                       cornerRadius:0
                               interpolationQuality:kCGInterpolationDefault];
    
    //    frameImage = [frameImage roundedCornerImage:3.0f borderSize:2.0f];
    self.posterImageView.image = thumbnail;
    
    
    
    self.posterImageView.image = thumbnail;
    self.posterImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.posterImageView.layer.borderWidth = 3.0f;
    [self.posterImageView.layer setCornerRadius:8.0f];
    [self.posterImageView.layer setMasksToBounds:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)onSave:(id)inSender {
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    [library writeImageToSavedPhotosAlbum:[self.posterImageView.image CGImage]
                              orientation:(ALAssetOrientation)[self.posterImageView.image imageOrientation]
                          completionBlock:^(NSURL *assetURL, NSError *error){
                              if (error) {
                                  // TODO: error handling
                              } else {
                                  // TODO: success handling
                                  [self showSavedSuccessMessage];
                              }
                          }];
}
- (void)showSavedSuccessMessage {
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setMessage:@"Photo saved successfully to camera roll."];
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}
- (void)saveAllPhotosToLibrary {
    self.toolBar.hidden = NO;
    self.progressBar.progress = 0.0f;
    
    if(self.frameSaveQueue) {
        self.frameSaveQueue = nil;
    }
    self.frameSaveQueue = [[NSOperationQueue alloc] init];
    self.frameSaveQueue.maxConcurrentOperationCount = 1;
    
//    NSString *framesDirPath = [[RIUtilities outputImageFrameIndex:0] stringByDeletingLastPathComponent];
    NSString *framesDirPath = [[RIUtilities framePathAtIndex:0 forFolder:self.folderName] stringByDeletingLastPathComponent];
    NSArray * framePaths = [[NSFileManager defaultManager] subpathsAtPath:framesDirPath];
    self.numberOfFrames = [framePaths count];
    NSInteger frameIndex = 0;
    for(NSString * framePath in framePaths) {
        if([[framePath pathExtension] isEqualToString:@"png"]) {
            VPFrameSaveOperation *saveOperation = [[VPFrameSaveOperation alloc] init];
            saveOperation.frameIndex = frameIndex;
            saveOperation.delegate = self;
            [self.frameSaveQueue addOperation:saveOperation];
            frameIndex++;
        }
    }
}
- (void)copyImageToPasteboard {
    UIImage *imageFile = [[UIImage alloc] initWithContentsOfFile:self.frameFilePath];
    [[UIPasteboard generalPasteboard] setImage:imageFile];
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setMessage:@"Photo is copied to pasteboard."];
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}
- (void)printImage {
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    UIImage *imageFile = [[UIImage alloc] initWithContentsOfFile:self.frameFilePath];
    if  (pic && [UIPrintInteractionController isPrintingAvailable] ) {
        //        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = [self.frameFilePath lastPathComponent];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        pic.printInfo = printInfo;
        pic.showsPageRange = YES;
        pic.printingItem = imageFile;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
            if (!completed && error)
                NSLog(@"FAILED! due to error in domain %@ with error code %u",
                      error.domain, error.code);
        };
        [pic presentAnimated:YES completionHandler:completionHandler];
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)shareWithMail {
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"Check out this image frame!"];
        UIImage *imageFile = [[UIImage alloc] initWithContentsOfFile:self.frameFilePath];
        NSData *imageData = UIImagePNGRepresentation(imageFile);
        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"Image"];
        [self presentViewController:mailer animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}
- (void)shareWithTwitter {
    ELSNSManager *snsManager = [[ELSNSManager alloc] init];
    UIImage *imageFile = [[UIImage alloc] initWithContentsOfFile:self.frameFilePath];
    [snsManager presentStatusUpdateSheetWithParameters:[NSDictionary dictionaryWithObjectsAndKeys:imageFile,@"IMAGE", nil]
                                            forService:eTwitter
                                          onController:self];
}
- (void)shareWithFacebook {
    ELSNSManager *snsManager = [[ELSNSManager alloc] init];
    UIImage *imageFile = [[UIImage alloc] initWithContentsOfFile:self.frameFilePath];
    [snsManager presentStatusUpdateSheetWithParameters:[NSDictionary dictionaryWithObjectsAndKeys:imageFile,@"IMAGE", nil]
                                            forService:eFacebook
                                          onController:self];
}
#pragma mark - Frame save operation delegate
- (void)operation:(VPFrameSaveOperation *)inOperation didFinishSavingFrameAtIndex:(NSInteger)inIndex {
    [self performSelectorOnMainThread:@selector(updateProgressWithIndex:) withObject:[NSNumber numberWithInteger:inIndex]
                        waitUntilDone:YES];
}
- (void)updateProgressWithIndex:(NSNumber *)inIndex {
    
    CGFloat progress = (float)([inIndex integerValue] + 1) / self.numberOfFrames;
    
    if(progress < self.progressBar.progress)
        return;
    
    [self.progressBar setProgress:progress
                         animated:YES];
    
    if([inIndex integerValue] + 1 == self.numberOfFrames) {
        toolBar.hidden = YES;
    }
}
#pragma mark - Tableview delegates & datasources
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 7;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    switch(indexPath.section) {
        case 0:
            cell.imageView.image = [UIImage imageNamed:@"camera.png"];
            cell.textLabel.text = @"Camera roll";
            break;
        case 1:
            cell.imageView.image = [UIImage imageNamed:@"facebook.png"];
            cell.textLabel.text = @"Facebook";
            break;
        case 2:
            cell.imageView.image = [UIImage imageNamed:@"twitter.png"];
            cell.textLabel.text = @"Twitter";
            break;
        case 3:
            cell.imageView.image = [UIImage imageNamed:@"Email-Icon.png"];
            cell.textLabel.text = @"Email";
            break;
        case 4:
            cell.imageView.image = [UIImage imageNamed:@"copy.png"];
            cell.textLabel.text = @"Copy";
            break;
        case 5:
            cell.imageView.image = [UIImage imageNamed:@"download.png"];
            cell.textLabel.text = @"Save all";
            break;
        case 6:
            cell.imageView.image = [UIImage imageNamed:@"print.png"];
            cell.textLabel.text = @"Print";
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            [self onSave:nil];
            break;
        case 1:
            [self shareWithFacebook];
            break;
        case 2:
            [self shareWithTwitter];
            break;
        case 3:
            [self shareWithMail];
            break;
        case 4:
            [self copyImageToPasteboard];
            break;
        case 5:
            [self saveAllPhotosToLibrary];
            break;
        case 6:
            [self printImage];
            break;
            
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

@end
