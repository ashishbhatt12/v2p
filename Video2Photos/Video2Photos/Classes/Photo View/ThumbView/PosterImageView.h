#import <UIKit/UIKit.h>

@class PosterImageView;

@protocol PosterImageViewSelectionDelegate <NSObject>
- (void)posterImageViewWasSelected:(PosterImageView *)posterImageView;
@end

@interface PosterImageView : UIImageView {

    UIImageView *highlightView;
    id <PosterImageViewSelectionDelegate> __weak delegate;
    NSInteger posterIndex;
}
@property(nonatomic, weak) id<PosterImageViewSelectionDelegate> __weak delegate;
@property(nonatomic,assign)NSInteger posterIndex;
@end
