#import "PosterImageView.h"

@interface PosterImageView()
-(void)createHighlightImageViewIfNecessary;
@end

@implementation PosterImageView
@synthesize delegate;
@synthesize posterIndex;
- (void)dealloc {
    self.delegate = nil;
//    [highlightView release];
    
//    [super dealloc];
}


#pragma mark -
#pragma mark Touch handling

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self createHighlightImageViewIfNecessary];
    [self addSubview:highlightView];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [highlightView removeFromSuperview];
    [delegate posterImageViewWasSelected:self];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [highlightView removeFromSuperview];
}


#pragma mark -
#pragma mark Helper methods

- (void)createHighlightImageViewIfNecessary {
    if (!highlightView) {
        UIImage *posterImageHighlight = [UIImage imageNamed:@"PosterHighlight"];
        highlightView = [[UIImageView alloc] initWithImage:posterImageHighlight];
        [highlightView setAlpha: 0.5];
    }
}


@end
