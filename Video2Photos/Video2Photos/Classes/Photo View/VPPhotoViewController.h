//
//  VPPhotoViewController.h
//  Video2Photos
//
//  Created by Ashish Bhatt on 12/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AwesomeMenu.h"
#import "AwesomeMenuItem.h"
#import "VPFrameSaveOperation.h"
#import "SWSnapshotStackView.h"
@interface VPPhotoViewController : UIViewController <VPFrameSaveDelegate,UIGestureRecognizerDelegate>{
    @private
    NSInteger photoIndex;
    NSString *frameFilePath;
    NSString *folderName;
}
@property(nonatomic,strong)UIImage *selectedImage;
@property(nonatomic,strong)NSString *folderName;
@property(nonatomic,strong)NSString *frameFilePath;
@property(nonatomic,assign)NSInteger photoIndex;
@property(nonatomic,weak)IBOutlet UIImageView * __weak posterImageView;
@property(nonatomic,weak)IBOutlet SWSnapshotStackView * __weak snapshotStackView;

@end
