//
//  VPFrameMovieView.h
//  Video2Photos
//
//  Created by Ashish on 12/7/12.
//
//

#import <UIKit/UIKit.h>
@class VPFrameMovieView;
@protocol VPFrameMovieViewDelegate
- (void)movieView:(VPFrameMovieView *)inView didMoveAtTime:(CGFloat)inTime;
@end
@interface VPFrameMovieView : UIView
@property(nonatomic,assign)IBOutlet NSObject<VPFrameMovieViewDelegate> *delegate;
@property(nonatomic,strong)NSURL *videoURL;
- (void)updatePlaybackTime:(CGFloat)inTime;
- (void)playTrack;
- (void)stopTrack;
- (void)viewWillAppear;
@end
