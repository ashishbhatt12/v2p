//
//  VPFrameMovieView.m
//  Video2Photos
//
//  Created by Ashish on 12/7/12.
//
//

#import "VPFrameMovieView.h"
#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>
@interface VPFrameMovieView()
@property(nonatomic,strong)MPMoviePlayerController *moviePlayerController;
@property(nonatomic,assign)CGFloat currentTime;
@property(nonatomic,strong)NSTimer *frameTimer;

@end

@implementation VPFrameMovieView
@synthesize videoURL = _videoURL;
@synthesize currentTime = _currentTime;
@synthesize frameTimer = _frameTimer;
@synthesize delegate = _delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)viewWillAppear {
    self.moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:self.videoURL];
    
    if (self.moviePlayerController != nil){
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(videoHasFinishedPlaying:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:self.moviePlayerController];
        
        NSLog(@"Successfully instantiated the movie player.");
        
        self.moviePlayerController.controlStyle = MPMovieControlStyleNone;
        self.moviePlayerController.shouldAutoplay = YES;
        
//        [self.moviePlayerController prepareToPlay];
        //        [self.moviePlayerController play];
        
        [self.moviePlayerController setCurrentPlaybackTime:0.0f];

//        [self playTrack];
        
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            [self.moviePlayerController.view setFrame:CGRectMake(0, 0, 768.0f, 1004.0f)];
        } else {
            [self.moviePlayerController.view setFrame:CGRectMake(51.0f, 20.0f, 220.0f, 320.0f)];
        }
        
        [self.moviePlayerController.view.layer setCornerRadius:5.0f];
        [self.moviePlayerController.view.layer setMasksToBounds:YES];
        
        [self addSubview:self.moviePlayerController.view];
        
        self.frameTimer = [NSTimer scheduledTimerWithTimeInterval:(1 / 30.0f)
                                                           target:self
                                                         selector:@selector(updateVideoFrame)
                                                         userInfo:nil
                                                          repeats:YES];

    }
}
- (void)updateVideoFrame {
    [self.delegate movieView:self didMoveAtTime:self.moviePlayerController.currentPlaybackTime];
    self.currentTime = self.moviePlayerController.currentPlaybackTime;
}
- (void)videoHasFinishedPlaying:(NSNotification *)paramNotification {
    /* Find out what the reason was for the player to stop */
    NSNumber *reason = [paramNotification.userInfo valueForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    if (reason != nil){
        NSInteger reasonAsInteger = [reason integerValue];
        switch (reasonAsInteger){
            case MPMovieFinishReasonPlaybackEnded:{
                /* The movie ended normally */
                break; }
            case MPMovieFinishReasonPlaybackError:{
                /* An error happened and the movie ended */
                break;
            }
            case MPMovieFinishReasonUserExited:{
                /* The user exited the player */
                break;
            }
        }
        NSLog(@"Finish Reason = %ld", (long)reasonAsInteger);
        self.moviePlayerController.currentPlaybackTime = self.moviePlayerController.duration;
    } /* if (reason != nil){ */
}
- (void)updatePlaybackTime:(CGFloat)inTime {
    self.moviePlayerController.currentPlaybackTime = inTime;

    if(inTime - self.currentTime > 0) {
        self.moviePlayerController.currentPlaybackRate = -1;
    } else {
        self.moviePlayerController.currentPlaybackRate = 1;
    }
    self.currentTime = inTime;
}
- (void)playTrack {
    self.frameTimer = [NSTimer scheduledTimerWithTimeInterval:(1 / 30.0f)
                                                       target:self
                                                     selector:@selector(updateVideoFrame)
                                                     userInfo:nil
                                                      repeats:YES];

    self.moviePlayerController.initialPlaybackTime = self.currentTime;
    [self.moviePlayerController play];
    

}
- (void)stopTrack {
    self.moviePlayerController.currentPlaybackTime = self.currentTime;
    [self.moviePlayerController pause];
    [self.frameTimer invalidate];
    self.frameTimer = nil;
}

@end
