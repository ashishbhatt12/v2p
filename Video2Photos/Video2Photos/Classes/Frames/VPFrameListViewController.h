//
//  VPFrameListViewController.h
//  Video2Photos
//
//  Created by Ashish on 13/12/12.
//
//

#import <UIKit/UIKit.h>
#include <AudioToolbox/AudioToolbox.h>
#import "AVPlayerDemoPlaybackViewController.h"
#import "FrameViewCell.h"
#import "MBProgressHUD.h"
@interface VPFrameListViewController : UIViewController<AVPlayerDemoPlaybackDelegate,MBProgressHUDDelegate> {
    @private
    CFURLRef		soundFileURLRef;
	SystemSoundID	soundFileObject;
    FrameViewCell *_frameCell;
    AVPlayerDemoPlaybackViewController *playbackViewController;
    NSString *framesFolderName;
    BOOL framesExist;
    BOOL landScapeVideo;
    MBProgressHUD *HUD;
    double prevContentOffestY;
    double currentDtsValue;
	NSCache 	*imageCache;    
}
@property(nonatomic,strong)	NSCache *imageCache;
@property(nonatomic,assign)double currentDtsValue;
@property(nonatomic,assign)CGSize videoSize;
@property(nonatomic,strong)MBProgressHUD *HUD;
@property(nonatomic,assign)BOOL landScapeVideo;
@property(nonatomic,assign)BOOL framesExist;
@property(nonatomic,strong)NSString *framesFolderName;
@property(nonatomic,strong)IBOutlet FrameViewCell *frameCell;
@property(nonatomic,assign)BOOL directStartFraming;
@property(nonatomic,strong)NSURL *videoURL;
@property (readwrite)	CFURLRef		soundFileURLRef;
@property (readonly)	SystemSoundID	soundFileObject;
@property(nonatomic,retain)    AVPlayerDemoPlaybackViewController *playbackViewController;
@end
