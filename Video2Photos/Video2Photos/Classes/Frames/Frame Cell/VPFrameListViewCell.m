//
//  VPFrameListViewCell.m
//  Video2Photos
//
//  Created by Ashish on 1/16/13.
//
//

#import "VPFrameListViewCell.h"
#import "RIUtilities.h"
NSString * const kJKPrepareForReuseNotification = @"JKCallbacksTableViewCell_PrepareForReuse";

@implementation VPFrameListViewCell
@synthesize frameView = _frameView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withLandscape:(BOOL)inLandscape
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        CGRect frameRect = CGRectZero;
        if(inLandscape) {
            frameRect = CGRectMake(40.0f, 5.0f, 240.0f, 115.0f);
        } else {
            frameRect = CGRectMake(40.0f, 5.0f, 240.0f, 310.0f);
        }
        self.frameView = [[UIImageView alloc] initWithFrame:frameRect];
        self.frameView.contentMode = UIViewContentModeScaleAspectFill;
//        self.frameView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [self.contentView addSubview:self.frameView];
        [self setBackgroundColor:[UIColor clearColor]];
        
        [[self frameView] addObserver:self
						   forKeyPath:@"image"
							  options:NSKeyValueObservingOptionOld
							  context:NULL];
        
        if([RIUtilities isIphoneDevice]) {
            [self.frameView.layer setCornerRadius:8.0f];
        } else {
            [self.frameView.layer setCornerRadius:11.0f];
        }
        [self.frameView.layer setMasksToBounds:YES];
        
        [self.frameView setOpaque:YES];
        [self setOpaque:YES];
        
    }
    return self;
}
// The reason we’re observing changes is that if you create a table view cell, return it to the
// table view, and then later add an image (perhaps after doing some background processing), you
// need to call -setNeedsLayout on the cell for it to add the image view to its view hierarchy. We
// asked the change dictionary to contain the old value because this only needs to happen if the
// image was previously nil.
- (void)observeValueForKeyPath:(NSString *)keyPath
					  ofObject:(id)object
						change:(NSDictionary *)change
					   context:(void *)context
{
	if (object == [self frameView] &&
		[keyPath isEqualToString:@"image"] &&
		([change objectForKey:NSKeyValueChangeOldKey] == nil ||
		 [change objectForKey:NSKeyValueChangeOldKey] == [NSNull null])) {
            [self setNeedsLayout];
        }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)prepareForReuse
{
	[[NSNotificationCenter defaultCenter] postNotificationName:kJKPrepareForReuseNotification
														object:self];
	
	[super prepareForReuse];
}
- (void)dealloc {
    [[self frameView] removeObserver:self forKeyPath:@"image" context:NULL];
}
@end
