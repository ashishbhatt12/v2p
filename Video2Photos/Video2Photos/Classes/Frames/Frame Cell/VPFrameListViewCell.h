//
//  VPFrameListViewCell.h
//  Video2Photos
//
//  Created by Ashish on 1/16/13.
//
//

#import <UIKit/UIKit.h>
extern NSString * const kJKPrepareForReuseNotification;

@interface VPFrameListViewCell : UITableViewCell
@property(nonatomic,strong)UIImageView *frameView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withLandscape:(BOOL)inLandscape;
@end
