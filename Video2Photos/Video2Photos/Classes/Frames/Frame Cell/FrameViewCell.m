//
//  FrameViewCell.m
//  Video2Photos
//
//  Created by Ashish on 29/09/12.
//
//

#import "FrameViewCell.h"
#import "RIUtilities.h"
#import "UIImage+Resize.h"
#import "EGOCache.h"

@implementation FrameViewCell
@synthesize imageView;
@synthesize backgroundImage;
@synthesize frameNumber;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)awakeFromNib {
    if(![RIUtilities isIphoneDevice]) {
        [self.backgroundImage setImage:[UIImage imageNamed:@"frame-strip-background@2x.png"]];
        [(UIImageView *)[self.contentView viewWithTag:7000] setImage:[UIImage imageNamed:@"frame-strip-background@2x.png"]];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setImageAtIndex:(NSInteger)inIndex underFolder:(NSString *)inFolder isLandscape:(BOOL)inLandscape{
    NSDictionary *infoDict = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:inIndex],@"INDEX",
                              inFolder,@"FOLDER",[NSNumber numberWithBool:inLandscape],@"LAND", nil];
    self.frameNumber.hidden = NO;
    [NSThread detachNewThreadSelector:@selector(fetchImageAtIndex:) toTarget:self withObject:infoDict];
}
- (void)fetchImageAtIndex:(NSDictionary *)inDict {
    @autoreleasepool {
        NSString *folder = [inDict objectForKey:@"FOLDER"];
        NSNumber *indexNumber = [inDict objectForKey:@"INDEX"];
        NSString *imagePath = [RIUtilities framePathAtIndex:[indexNumber integerValue] forFolder:folder];
        UIImage * frameImage = [[UIImage alloc] initWithContentsOfFile:imagePath];
                
        [self performSelectorOnMainThread:@selector(loadImage:) withObject:frameImage waitUntilDone:NO];
    }
}

- (void)loadImage:(UIImage *)inImage {
    self.frameNumber.hidden = YES;
    self.imageView.image = inImage;    
    if([RIUtilities isIphoneDevice]) {
        [self.imageView.layer setCornerRadius:8.0f];
        [self.imageView.layer setMasksToBounds:YES];
    } else {
        [self.imageView.layer setCornerRadius:11.0f];
        [self.imageView.layer setMasksToBounds:YES];
    }

}
@end
