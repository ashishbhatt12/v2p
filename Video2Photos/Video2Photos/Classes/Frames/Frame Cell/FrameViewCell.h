//
//  FrameViewCell.h
//  Video2Photos
//
//  Created by Ashish on 29/09/12.
//
//

#import <UIKit/UIKit.h>

@interface FrameViewCell : UITableViewCell {

}
@property(nonatomic,strong)IBOutlet UIImageView *backgroundImage;
@property(nonatomic,strong)IBOutlet UIImageView *imageView;
@property(nonatomic,strong)IBOutlet UILabel *frameNumber;
@property(nonatomic,assign)BOOL isLoaded;
- (void)setImageAtIndex:(NSInteger)inIndex
            underFolder:(NSString *)inFolder
            isLandscape:(BOOL)inLandscape;
- (void)loadImage:(UIImage *)inImage;
@end
