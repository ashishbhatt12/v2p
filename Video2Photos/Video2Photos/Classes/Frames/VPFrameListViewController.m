//
//  VPFrameListViewController.m
//  Video2Photos
//
//  Created by Ashish on 13/12/12.
//
//

#import "VPFrameListViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "VPFrameExtractController.h"
#import "VPPhotoViewController.h"
#import "VPPhotoViewController_ipad.h"
#import "DAAnisotropicImage.h"
#import "RIUtilities.h"
#import <MediaPlayer/MediaPlayer.h>
#import "VPVideoFrameGenerator.h"
#import "EGOCache.h"
#import "VPFrameListViewCell.h"
#import <objc/runtime.h>
#import <SDWebImage/SDImageCache.h>
#import "VPImageGenerateHandler.h"

static char * const kIndexPathAssociationKey = "JK_indexPath";

@interface VPFrameListViewController () <VPFrameExtractDelegate,VPVideoFrameGenerateDelegate,VPImageGeneratorDatasource,VPImageGeneratorDelegate>
@property(nonatomic,strong)IBOutlet UITableView *frameListView;
@property(nonatomic,weak)IBOutlet UISlider *frameSlider;
@property(nonatomic,weak)IBOutlet UIToolbar *frameToolbar;
@property(nonatomic,weak)IBOutlet UILabel *frameCounterLabel;
@property(nonatomic,weak)IBOutlet UILabel *frameTimeLabel;
@property(nonatomic,assign)NSInteger numberOfFrames;
@property(nonatomic,assign) CGFloat totalDuration;
@property(nonatomic,strong)VPFrameExtractController *frameExtractor;
@property(nonatomic,assign)BOOL extractFrames;
@property(nonatomic,strong)UIPopoverController *popViewController;
@property(nonatomic,strong)VPVideoFrameGenerator *videoFrameGenerator;
@property(nonatomic,strong)VPImageGenerateHandler *imageFrameGenerator;
@property(nonatomic,assign)BOOL framesThroughScroll;
@end

@implementation VPFrameListViewController
@synthesize frameCell = _frameCell;
@synthesize frameListView = _frameListView;
@synthesize numberOfFrames = _numberOfFrames;
@synthesize directStartFraming = _directStartFraming;
@synthesize videoURL = _videoURL;
@synthesize frameExtractor = _frameExtractor;
@synthesize frameSlider = _frameSlider;
@synthesize frameToolbar = _frameToolbar;
@synthesize frameCounterLabel = _frameCounterLabel;
@synthesize frameTimeLabel = _frameTimeLabel;
@synthesize soundFileURLRef;
@synthesize soundFileObject;
@synthesize totalDuration = _totalDuration;
@synthesize playbackViewController;
@synthesize extractFrames = _extractFrames;
@synthesize framesFolderName = _framesFolderName;
@synthesize popViewController = _popViewController;
@synthesize framesExist = _framesExist;
@synthesize videoFrameGenerator = _videoFrameGenerator;
@synthesize imageFrameGenerator = _imageFrameGenerator;
@synthesize landScapeVideo = _landScapeVideo;
@synthesize HUD;
@synthesize videoSize = _videoSize;
@synthesize currentDtsValue;
@synthesize framesThroughScroll = _framesThroughScroll;
@synthesize imageCache = _imageCache;

#pragma mark - Initial Methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning {
	[self.imageCache removeAllObjects];
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc {
    self.videoFrameGenerator = nil;
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] clearDisk];
    
    [self.imageCache removeAllObjects];
    self.imageCache = nil;
    self.playbackViewController = nil;
    self.frameExtractor = nil;
    AudioServicesDisposeSystemSoundID (soundFileObject);
}
- (void)awakeFromNib
{
	if (!self.imageCache) {
		self.imageCache = [[NSCache alloc] init];
		[self.imageCache setName:@"JKImageCache"];
	}
	
	[super awakeFromNib];
}
#pragma mark - View life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if(!self.imageCache) {
        self.imageCache = [[NSCache alloc] init];
        [self.imageCache setName:@"JKImageCache"];
    }
    
    // Register for our table view cell notification
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(tableViewCellIsPreparingForReuse:)
												 name:kJKPrepareForReuseNotification
											   object:nil];
    
    self.frameToolbar.hidden = NO;
    self.title = @"Loading...";
    self.extractFrames = YES;
    if([RIUtilities isIphoneDevice]) {
        self.frameListView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"frame-strip-background.png"]];
    } else {
        self.frameListView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"frame-strip-background@2x.png"]];        
    }
#if LITE_VERSION
    NSInteger totalCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"TOTAL_COUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:totalCount + 1 forKey:@"TOTAL_COUNT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
#endif

    // Configure movie playback
    [self configureMoviePlayback];
    
    // Custom Slider
    [self customizeSlider];
    
    // Video List button
    [self loadVideoListButton];
    
    // Configure Audio
    [self configureAudio];
    
    // Start video framing
    [self decideVideoFraming];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    [[EGOCache globalCache] clearCache];
//    self.frameListView.userInteractionEnabled = YES;
}
#pragma mark - Custom UI methods
- (void)customizeSlider {
    UIImage *stretchableFillImage = [[UIImage imageNamed:@"slider-fill.png"] stretchableImageWithLeftCapWidth:10.0f
                                                                                                 topCapHeight:0.0f];
    UIImage *stretchableTrackImage = [[UIImage imageNamed:@"slider-track.png"] stretchableImageWithLeftCapWidth:10.0f
                                                                                                   topCapHeight:0.0f];
    [self.frameSlider setMinimumTrackImage:stretchableFillImage forState:UIControlStateNormal];
    [self.frameSlider setMaximumTrackImage:stretchableTrackImage forState:UIControlStateNormal];

    UIImage *initialImage = [DAAnisotropicImage imageFromAccelerometerData:nil];
    [self.frameSlider setThumbImage:initialImage forState:UIControlStateNormal];
    [self.frameSlider setThumbImage:initialImage forState:UIControlStateHighlighted];
}
- (void)loadVideoListButton {
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0.0f, 0.0f, 32.0f, 32.0f);
    [backButton addTarget:self action:@selector(onBack:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:@"Clapboard.png"] forState:UIControlStateNormal];
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [container addSubview:backButton];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:container];
}
- (void)onBack:(id)inSender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)showActivityIndicator:(BOOL)inShow {
    if(inShow) {
        [self hideHUD];
        self.HUD = [[MBProgressHUD alloc] initWithView:self.view];
//        self.HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self.view addSubview:self.HUD];

        [self.HUD setRemoveFromSuperViewOnHide:YES];
        // Set determinate mode
        self.HUD.mode = MBProgressHUDModeDeterminate;
        
//        self.HUD.delegate = self;
        self.HUD.labelText = @"Loading";
        [self.HUD show:YES];
        
        // myProgressTask uses the HUD instance to update progress
//        [self.HUD showWhileExecuting:@selector(hudProgressTask:) onTarget:self withObject:nil animated:YES];
//        [self.HUD show:YES];
//        [self.HUD showAnimated:YES whileExecutingBlock:nil];
        
    } else {
//        self.navigationItem.rightBarButtonItem = nil;
        [self performSelectorOnMainThread:@selector(hideHUD) withObject:nil waitUntilDone:YES];
    }
}
- (void)hideHUD {
    if(self.HUD) {
        [self.HUD hide:YES];
    //    [self.HUD removeFromSuperview];
        self.HUD = nil;
    }
}
- (void)hudProgressTask:(CGFloat)inProgresss {
	// This just increases the progress indicator in a loop
//	float progress = 0.0f;
//	while (progress < 1.0f) {
//		progress += 0.01f;
		self.HUD.progress = inProgresss;
//		usleep(10000);
//	}
}
- (void)configureAudio {
    NSURL *tapSound   = [[NSBundle mainBundle] URLForResource: @"Frame_Selection"
                                                withExtension: @"mp3"];
    
    // Store the URL as a CFURLRef instance
    self.soundFileURLRef = (__bridge CFURLRef) tapSound;
    
    // Create a system sound object representing the sound file.
    AudioServicesCreateSystemSoundID (
                                      soundFileURLRef,
                                      &soundFileObject
                                      );

}
- (void)showFramingVideo:(BOOL)inShow {
    if(inShow) {
        [self configureVideoPlayback];
        [self.view addSubview:self.playbackViewController.view];
        [self.view bringSubviewToFront:self.frameToolbar];
        
        if(self.landScapeVideo) {
            [self.playbackViewController updateVideoFrame];
        }
    } else {
        [self updateFramePosition];
        [self.playbackViewController.view removeFromSuperview];
    }
}
- (void)updateFramePosition {
    // (1.3 * 100) / 3.25 ==>> (currentTime * 100) / TotalTime

    NSInteger frameIndex = (self.numberOfFrames  * self.frameSlider.value) / self.frameSlider.maximumValue;

    if(frameIndex >= self.numberOfFrames) {
        frameIndex = self.numberOfFrames - 1;
    }
    [self.frameListView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:frameIndex - 1 inSection:0]
                              atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
    [self loadFramesForVisibleRows];
}
- (void)adjustScrollPosition:(BOOL)inCenter {
    CGFloat currentPercent = (self.frameSlider.value * 100) / self.frameSlider.maximumValue;
    NSInteger newFrameToScroll = (self.frameExtractor.totalNumberFrames * currentPercent) / 100;
    if(newFrameToScroll >= self.numberOfFrames)
        newFrameToScroll = self.numberOfFrames - 1;
    
//    NSLog(@"New Frame : %d || Total : %d",newFrameToScroll,self.numberOfFrames);

    self.extractFrames = NO;
    [self.frameListView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:newFrameToScroll  inSection:0]
                              atScrollPosition:UITableViewScrollPositionNone
                                      animated:NO];
    
    if(inCenter) {
        NSArray *visiblaeCells = [self.frameListView visibleCells];
        if([visiblaeCells count]) {
            UITableViewCell *currentCell = [visiblaeCells objectAtIndex:0];
            NSIndexPath * currentIndexPath = [self.frameListView indexPathForCell:currentCell];
            [self.frameListView scrollToRowAtIndexPath:currentIndexPath
                                      atScrollPosition:UITableViewScrollPositionNone
                                              animated:NO];
        }
    }
    self.extractFrames = YES;
    /*
    return;
    double yOffset =  (self.frameListView.contentSize.height * currentPercent) / 100.0f;
    
    if(yOffset >= self.frameListView.contentSize.height) {
        if([RIUtilities isIphoneDevice]) {
            yOffset = self.frameListView.contentSize.height - 416.0f;
        } else {
            yOffset = self.frameListView.contentSize.height - 950.0f;
        }
    }
    [self.frameListView setContentOffset:CGPointMake(self.frameListView.contentOffset.x,
                                                     yOffset) animated:NO];
    
    
    NSArray *visiblaeCells = [self.frameListView visibleCells];
    if([visiblaeCells count]) {
        UITableViewCell *currentCell = [visiblaeCells objectAtIndex:0];
        NSIndexPath * currentIndexPath = [self.frameListView indexPathForCell:currentCell];
        [self.frameListView scrollToRowAtIndexPath:currentIndexPath
                                  atScrollPosition:UITableViewScrollPositionNone
                                          animated:NO];
    }
     */
}
- (void)updateFrameCounter {
    CGFloat totalTrackDuration = self.totalDuration;
    double currentTime =  [self.playbackViewController currentPlaybackTime];
    NSInteger currentFrame = 0;
    if(isnan(currentTime) || ![self.playbackViewController isPlaying]) {
        CGFloat currentPercent = (self.frameSlider.value * 100) / self.frameSlider.maximumValue;
        currentFrame = (self.numberOfFrames * currentPercent) / 100;
        
        /*
        NSArray *visiblaeCells = [self.frameListView visibleCells];
        if([visiblaeCells count]) {
            UITableViewCell *currentCell = [visiblaeCells objectAtIndex:0];
            NSIndexPath * currentIndexPath = [self.frameListView indexPathForCell:currentCell];
            currentFrame = currentIndexPath.row + 1;
        }
        */
    } else {
        currentFrame = (NSInteger)(currentTime * FramePerSeconds);
    }
    
    NSInteger totolFrames = (NSInteger)(totalTrackDuration * FramePerSeconds);
    if(currentFrame > totolFrames)
        return;
    if(currentFrame != totolFrames) {
        currentFrame--;
    }

    if(currentFrame <= 0)
        currentFrame = 1;
    self.frameTimeLabel.hidden = NO;
    self.frameCounterLabel.hidden = NO;
    self.frameCounterLabel.text = [NSString stringWithFormat:@"%d",currentFrame];
    self.frameTimeLabel.text = [NSString stringWithFormat:@"%d",totolFrames];
}

#pragma mark - tableView delegates & dataSources
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.numberOfFrames;
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CellIdentifier";
    FrameViewCell *cell = (FrameViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell) {
        [[NSBundle mainBundle] loadNibNamed:@"FrameViewCell" owner:self options:nil];
        cell = self.frameCell;
        self.frameCell = nil;
    }
    if(!tableView.decelerating && !tableView.dragging) {
        [cell setImageAtIndex:indexPath.row underFolder:self.framesFolderName isLandscape:self.landScapeVideo];
    } 
    cell.frameNumber.text = [NSString stringWithFormat:@"%d",indexPath.row + 1];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CellIdentifier";
    VPFrameListViewCell *cell = (VPFrameListViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell) {
        cell = [[VPFrameListViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier withLandscape:self.landScapeVideo];
    }
    
//    if(tableView.decelerating)// || tableView.dragging)
//        return cell;
    // If we already have an image cached for the cell, use that. Otherwise we need to go into the
	// background and generate it.
//    NSString *imagePath = [RIUtilities framePathAtIndex:indexPath.row forFolder:self.framesFolderName];
//    NSString * imageFilename = [imagePath lastPathComponent];
//	UIImage *image = [imageCache objectForKey:[NSString stringWithFormat:@"%d",indexPath.row]];
    
    UIImage *image = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:[NSString stringWithFormat:@"%d",indexPath.row]];
    
    if (image) {
		[[cell frameView] setImage:image];
	} else {
//        self.videoFrameGenerator.numberOfThumbsNeeded = 1;
//        [self.videoFrameGenerator initiateThumbnailFramingAtIndex:indexPath.row];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([RIUtilities isIphoneDevice]) {
        if(!self.landScapeVideo) {
            return 320.0f;//362.0f;
        }
        else {
            return 125.0f;//362.0f;
        }
    } else {
        return 460.0f;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *imagePath = [RIUtilities framePathAtIndex:indexPath.row forFolder:self.framesFolderName];
//    if(![[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
    if(![[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:[NSString stringWithFormat:@"%d",indexPath.row]]) {
        return;
    }
    AudioServicesPlayAlertSound (soundFileObject);
    if([RIUtilities isIphoneDevice]) {
        VPPhotoViewController *photoVideoController = [[VPPhotoViewController alloc] init];
        photoVideoController.folderName = self.framesFolderName;
        photoVideoController.selectedImage = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:[NSString stringWithFormat:@"%d",indexPath.row]];
        photoVideoController.frameFilePath = [RIUtilities framePathAtIndex:indexPath.row forFolder:self.framesFolderName];
        photoVideoController.photoIndex = indexPath.row;
        [self.navigationController pushViewController:photoVideoController animated:YES];
    } else {
        VPPhotoViewController_ipad *photoViewController = [[VPPhotoViewController_ipad alloc] init];
        photoViewController.folderName = self.framesFolderName;
        photoViewController.selectedImage = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:[NSString stringWithFormat:@"%d",indexPath.row]];        
        photoViewController.frameFilePath = [RIUtilities framePathAtIndex:indexPath.row forFolder:self.framesFolderName];        
        photoViewController.photoIndex = indexPath.row;
        
        self.popViewController = [[UIPopoverController alloc] initWithContentViewController:photoViewController];
        [self.popViewController setPopoverContentSize:CGSizeMake(600.0f, 440.0f) animated:YES];
        [self.popViewController presentPopoverFromRect:[tableView rectForRowAtIndexPath:indexPath]
                                                inView:self.view
                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                              animated:YES];
    }
}
- (void)loadFramesForVisibleRows {
    NSArray *visibleCells = [self.frameListView indexPathsForVisibleRows];
    
    if([visibleCells count]) {
        NSIndexPath *lastIndexPath = [visibleCells objectAtIndex:0];
        if(0) {
            self.videoFrameGenerator.numberOfThumbsNeeded = FRAMES_TO_CAPTURE;
            self.framesThroughScroll = YES;
            [self.videoFrameGenerator initiateThumbnailFramingAtIndex:lastIndexPath.row];
        } else {
            [self.imageFrameGenerator generateFramesAtIndex:lastIndexPath.row];
        }
    }
}
#pragma mark - Scrollview delegates
//- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
//                     withVelocity:(CGPoint)velocity
//              targetContentOffset:(inout CGPoint *)targetContentOffset {
//    NSLog(@" Dragging velocity : %@",NSStringFromCGPoint(velocity));
//    if (!scrollView.decelerating) {
//        [self loadFramesForVisibleRows];
//    }
//}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self loadFramesForVisibleRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self loadFramesForVisibleRows];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSArray *visibleCells = [self.frameListView indexPathsForVisibleRows];
    if([visibleCells count]) {
        NSIndexPath * currentIndexPath = [visibleCells lastObject];
        NSInteger rowIndex = currentIndexPath.row + 1;
        CGFloat currentPercent = (rowIndex * 100.0f) / self.numberOfFrames;
        CGFloat sliderValue = (self.frameSlider.maximumValue *currentPercent) / 100.0f;
        [self.frameSlider setValue:sliderValue animated:YES];
        [self updateFrameCounter];
    }
}
#pragma mark - Video framing
- (void)decideVideoFraming {
    self.title = @"Loading...";
//    [self showActivityIndicator:YES];
    if(self.directStartFraming) {
        [self extractVideoFrames];
    } else {

        [self saveVideoToLocally];
    }
}
- (void)saveVideoToLocally {
    @autoreleasepool {
        ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
        [assetLibrary assetForURL:self.videoURL resultBlock:^(ALAsset *asset) {
            ALAssetRepresentation *rep = [asset defaultRepresentation];
            Byte *buffer = (Byte*)malloc(rep.size);
            NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
            NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
            NSString *videoPath = [[RIUtilities appDocumentsDirectory] stringByAppendingPathComponent:@"SampleVideo.mov"];
            if([[NSFileManager defaultManager] fileExistsAtPath:videoPath]) {
                [[NSFileManager defaultManager] removeItemAtPath:videoPath error:nil];
            }
            [[NSFileManager defaultManager] createFileAtPath:videoPath contents:data attributes:nil];
            self.videoURL = [NSURL fileURLWithPath:videoPath];
            [self performSelectorOnMainThread:@selector(extractVideoFrames) withObject:nil waitUntilDone:YES];
        } failureBlock:^(NSError *err) {
            NSLog(@"Error: %@",[err localizedDescription]);
        }];
    }
}
- (UIImage *)imageFromMovie:(NSURL *)movieURL atTime:(NSTimeInterval)time {
    // set up the movie player
    MPMoviePlayerController *mp = [[MPMoviePlayerController alloc]
                                   initWithContentURL:movieURL];
    mp.shouldAutoplay = NO;
    mp.initialPlaybackTime = time;
    mp.currentPlaybackTime = time;
    // get the thumbnail
    UIImage *thumbnail = [mp thumbnailImageAtTime:time
                                       timeOption:MPMovieTimeOptionExact];
    
    
    // clean up the movie player
    [UIImagePNGRepresentation(thumbnail) writeToFile:[[RIUtilities appDocumentsDirectory]
                                                      stringByAppendingPathComponent:[NSString stringWithFormat:@"%f-ABC.png",time]]
                                          atomically:YES];
    [mp stop];
    //    [mp release];
    return(thumbnail);
}

- (void)extractVideoFrames {
    self.title = @"Photo selection";
    // Check video orientation
    self.landScapeVideo = [RIUtilities isLandscapeVideo:self.videoURL];
    
    // Device video duration
    [self setVideoDuration];

    // Calculate total number of frames
    self.numberOfFrames = self.totalDuration * FramePerSeconds;
    [self.frameListView reloadData];
#if OPTIMIZATION
    if(1) {
        [self configureImageGenerator];
        [self.imageFrameGenerator generateFramesAtIndex:0];
    } else {
        [self configureFrameGenerator];
        if(self.landScapeVideo) {
            self.videoFrameGenerator.numberOfThumbsNeeded = FRAMES_TO_CAPTURE;
        } else {
            self.videoFrameGenerator.numberOfThumbsNeeded = FRAMES_TO_CAPTURE;
        }
        self.framesThroughScroll = NO;
        [self.videoFrameGenerator initiateThumbnailFramingAtIndex:0];
    }
#else
    // Configure frame Extractor
    [self configureFrameExtractor];
//    if(self.landScapeVideo) {
        self.frameExtractor.framesToCompare = 5;
//    } else {
//        self.frameExtractor.framesToCompare = 3;
//    }
    [self.frameExtractor startFrameExtractionFromIndex:0];
#endif
}
#pragma mark - video frame extract delegates
- (CGSize)calculatedSizeForVideoFrames {
    return self.videoSize;
}
- (double)timeDurationSelectedVideo:(VPFrameExtractController *)inController {
    return self.totalDuration;
}
- (void)didStartExtractingFramesWithController:(VPFrameExtractController *)inController {
//    if(!self.frameExtractor.framingFromScroll)
//        [self showActivityIndicator:YES];
}
- (void)controller:(VPFrameExtractController *)inController didExtractFrameAtIndex:(NSInteger)inIndex {
    
    if(inIndex < self.numberOfFrames) {
        [self.frameListView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:inIndex inSection:0]]
                                  withRowAnimation:UITableViewRowAnimationNone];
    }
}
- (void)didFinishExtractingFramesWithController:(VPFrameExtractController *)inController {
    self.title = @"Photo selection";
    self.currentDtsValue = inController.currentDtsValue;
    [self showActivityIndicator:NO];
    if(self.frameToolbar.hidden)
        self.frameToolbar.hidden = NO;
    self.frameExtractor.framingFromScroll = NO;
//    [self adjustScrollPosition:YES];
//    [self performSelector:@selector(cleanFrameExtractor) withObject:nil afterDelay:0.0];
    [self.frameExtractor stopFramingProcess];
}
- (void)cleanFrameExtractor {
    self.frameExtractor = nil;
}
#pragma mark - Video playback
- (void)configureMoviePlayback {
    self.playbackViewController = [[AVPlayerDemoPlaybackViewController alloc] init];
    self.playbackViewController.landScapeVideo = self.landScapeVideo;
    self.playbackViewController.delegate = self;
}

- (void)setVideoDuration {
    AVURLAsset *videoAsset = [[AVURLAsset alloc] initWithURL:self.videoURL options:nil];
    CGFloat seconds = CMTimeGetSeconds(videoAsset.duration);
    self.totalDuration = seconds;
    CGSize frameSize = CGSizeMake(480.0f, 320.0f);
    // Decide size of frames to be taken
    if([[videoAsset tracksWithMediaType:AVMediaTypeVideo] count]) {
        AVAssetTrack *videoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];

        if(frameSize.width > videoTrack.naturalSize.width) {
            frameSize.width = videoTrack.naturalSize.width;
        }
        if(frameSize.height > videoTrack.naturalSize.height) {
            frameSize.height = videoTrack.naturalSize.height;
        }
//        NSLog(@"Natural size : %@ || Decided Size : %@",NSStringFromCGSize(videoTrack.naturalSize),NSStringFromCGSize(frameSize));
    }
    self.videoSize = frameSize;
}
- (void)configureVideoPlayback {
    if(self.frameSlider.value == self.frameSlider.maximumValue) {
        self.playbackViewController.startTime = kCMTimeZero;
    } else {
        CGFloat currentPercent = (self.frameSlider.value * 100) / self.frameSlider.maximumValue;
        CGFloat currentTime = (currentPercent * self.totalDuration) / 100.0f;
        self.playbackViewController.startTime = CMTimeMakeWithSeconds(currentTime, 600);
    }
    
    if(self.playbackViewController.URL) {
        if(self.playbackViewController.autoPlay)
            [self.playbackViewController play:nil];
    } else {
        [self.playbackViewController setURL:self.videoURL];
    }
}
- (IBAction)stopVideo:(id)sender {
    self.playbackViewController.autoPlay = NO;
    [self.playbackViewController pause:nil];
    [self showFramingVideo:NO];
    
    NSArray * items = [self.frameToolbar items];
    NSMutableArray *newItems = [NSMutableArray arrayWithObjects:[[UIBarButtonItem alloc]
                                                                 initWithBarButtonSystemItem:UIBarButtonSystemItemPlay
                                                                 target:self
                                                                 action:@selector(playVideo:)],[items objectAtIndex:1],[items objectAtIndex:2],[items objectAtIndex:3], nil];
    [self.frameToolbar setItems:newItems animated:NO];
}
- (IBAction)playVideo:(id)sender {
    
    self.playbackViewController.autoPlay = YES;
    [self showFramingVideo:YES];
    
    NSArray * items = [self.frameToolbar items];
    NSMutableArray *newItems = [NSMutableArray arrayWithObjects:[[UIBarButtonItem alloc]
                                                                 initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                 target:self
                                                                 action:@selector(stopVideo:)],[items objectAtIndex:1],
                                [items objectAtIndex:2],[items objectAtIndex:3], nil];
    [self.frameToolbar setItems:newItems animated:NO];
}

- (IBAction)beginScrubbing:(id)sender {
    self.playbackViewController.autoPlay = NO;
    [self showFramingVideo:YES];
    [self.playbackViewController beginScrubbing:nil];
}
- (IBAction)scrub:(id)sender {
    [self.playbackViewController scrub:sender];
   [self updateFrameCounter];
}
- (IBAction)endScrubbing:(id)sender {
    self.playbackViewController.autoPlay = YES;
    if(![self.playbackViewController isPlaying])
        [self showFramingVideo:NO];
    [self.playbackViewController endScrubbing:nil];
}

#pragma mark - VPFrameMovieViewDelegate
- (void)didFinishPlayingVideo:(AVPlayerDemoPlaybackViewController *)inPlayer {
    [self stopVideo:nil];
}
- (void)player:(AVPlayerDemoPlaybackViewController *)inPlayer minimum:(CGFloat)inMin maximum:(CGFloat)inMax {
    self.frameSlider.minimumValue = inMin;
    self.frameSlider.maximumValue = inMax;
}
- (void)player:(AVPlayerDemoPlaybackViewController *)inPlayer didUpdateProgress:(CGFloat)inProgress {
    [self.frameSlider setValue:inProgress];
    [self updateFrameCounter];
}
#pragma mark - Optimized approach to extract frames
#pragma mark -
- (void)configureFrameGenerator {
    self.videoFrameGenerator = [[VPVideoFrameGenerator alloc] initWithVideo:self.videoURL];
    self.videoFrameGenerator.folderName = self.framesFolderName;
    self.videoFrameGenerator.delegate = self;
}
- (void)configureFrameExtractor {
    self.frameExtractor = [[VPFrameExtractController alloc] initWithVideoURL:self.videoURL];
    self.frameExtractor.totalNumberFrames = self.numberOfFrames;
    self.frameExtractor.landScapeVideo = self.landScapeVideo;
    self.frameExtractor.folderName = self.framesFolderName;
    self.frameExtractor.currentDtsValue = self.currentDtsValue;
    self.frameExtractor.delegate = self;
}
#pragma mark - frame extract delegate
- (void)controller:(VPFrameExtractController *)inController didGenerateFrame:(NSInteger)inIndex {
}
- (void)controller:(VPFrameExtractController *)inController didFinishPercent:(CGFloat)inPercent {
//    self.HUD.progress = inPercent;
    [self hudProgressTask:inPercent];
}
#pragma mark - VPVideoFrameGenerator
- (void)didStartFramingWithGenerator:(VPVideoFrameGenerator *)inGenerator {
    if(!self.framesThroughScroll) {
//        [self showActivityIndicator:YES];
        [self updateFrameCounter];
    }
}
/*
- (void)generator:(VPVideoFrameGenerator *)inGenerator didGenerateFrame:(UIImage *)inFrame forIndex:(NSInteger)inIndex finishedPercent:(CGFloat)inPercent{
    [self hudProgressTask:inPercent];
    if(inIndex < self.numberOfFrames) {
        FrameViewCell *cell = (FrameViewCell *)[self.frameListView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:inIndex inSection:0]];
        if(!cell.imageView.image) {
            [cell loadImage:inFrame];
//            [self.frameListView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:inIndex inSection:0]]
//                                      withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}
*/
- (void)generator:(VPVideoFrameGenerator *)inGenerator didGenerateFrame:(UIImage *)inFrame forIndex:(NSInteger)inIndex finishedPercent:(CGFloat)inPercent{
    NSLog(@"Loaded frame : %d",inIndex);
    if(inFrame) {   
        if(inIndex < self.numberOfFrames) {
            VPFrameListViewCell *cell = (VPFrameListViewCell *)[self.frameListView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:inIndex inSection:0]];
//            if(!cell.frameView.image) {
                [cell.frameView setImage:inFrame];
//                [self.frameListView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:inIndex inSection:0]]
//                                          withRowAnimation:UITableViewRowAnimationNone];
//            }
        }
    }
}

- (void)didFinishFramingWithGenerator:(VPVideoFrameGenerator *)inGenerator {
    self.title = @"Photo selection";
    [self showActivityIndicator:NO];
    self.framesThroughScroll = NO;

    if(self.frameToolbar.hidden)
        self.frameToolbar.hidden = NO;
    
    [self reloadVisibleCells];
}
- (void)reloadVisibleCells {
    NSArray *visibleCells = [self.frameListView indexPathsForVisibleRows];
    
    for(NSInteger index = 0; index < [visibleCells count]; index++) {
        NSIndexPath *lastIndexPath = [visibleCells objectAtIndex:index];
        VPFrameListViewCell *cell = (VPFrameListViewCell *)[self.frameListView cellForRowAtIndexPath:lastIndexPath];
        if(!cell.frameView.image) {
            NSString *imagePath = [RIUtilities framePathAtIndex:lastIndexPath.row forFolder:self.framesFolderName];
            if(![[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
                [self.frameListView reloadRowsAtIndexPaths:[NSArray arrayWithObject:lastIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
        }
    }
}
- (void)tableViewCellIsPreparingForReuse:(NSNotification *)notification {
    if ([[notification object] isKindOfClass:[VPFrameListViewCell class]]) {
		VPFrameListViewCell *cell = (VPFrameListViewCell *)[notification object];
		
		objc_setAssociatedObject(cell,
								 kIndexPathAssociationKey,
								 nil,
								 OBJC_ASSOCIATION_RETAIN);
		
		[[cell frameView] setImage:nil];
	}
}
#pragma mark - VPImageGenerateHandler
- (void)configureImageGenerator {
    self.imageFrameGenerator = [[VPImageGenerateHandler alloc] initWithAssetURL:self.videoURL];
    self.imageFrameGenerator.landscapeVideo = self.landScapeVideo;
    self.imageFrameGenerator.delegate = self;
    self.imageFrameGenerator.datasource = self;
}
#pragma mark - VPImageGenerateHandler delegates & datasources 
- (void)handler:(VPImageGenerateHandler *)inHandler didFindImage:(UIImage *)inImage forIndex:(NSInteger)inIndex {
//    NSLog(@"Loaded frame : %d",inIndex);
    if(inImage) {
        if(inIndex < self.numberOfFrames) {
            VPFrameListViewCell *cell = (VPFrameListViewCell *)[self.frameListView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:inIndex inSection:0]];
            [cell.frameView setImage:inImage];
        }
    }
}
- (void)didStartGeneratingImageSequence:(VPImageGenerateHandler *)inHandler {
}
- (void)didFinishGeneratingImageSequence:(VPImageGenerateHandler *)inHandler {
    
}
- (CMTime)videoDurationForImageGenerator:(VPImageGenerateHandler *)inGenerator {
    return CMTimeMakeWithSeconds(self.totalDuration, 600);
}
- (double)secondsForFrameIndex:(NSInteger)inIndex forImageGenerator:(VPImageGenerateHandler *)inGenerator {
    
    double seconds = (double)((double)(inIndex * self.totalDuration) / (double)(self.numberOfFrames - 1));
    return seconds;
}
@end
