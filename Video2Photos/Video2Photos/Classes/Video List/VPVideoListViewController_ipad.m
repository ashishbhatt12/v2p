//
//  VPVideoListViewController_ipad.m
//  Video2Photos
//
//  Created by Ashish on 01/10/12.
//
//

#import "VPVideoListViewController_ipad.h"

@interface VPVideoListViewController_ipad ()

@end

@implementation VPVideoListViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
