//
//  VPVideoListViewController_iphone.m
//  Video2Photos
//
//  Created by Ashish on 01/10/12.
//
//

#import "VPVideoListViewController_iphone.h"
#import "AssetsGroupsTableViewCell.h"
#import "AssetItem.h"
#import "RIImageGenerateHandler.h"
#import "VPFrameListViewController_iphone.h"
#import "RIUtilities.h"
#import <AVFoundation/AVFoundation.h>
@interface VPVideoListViewController_iphone () <AssetsGroupsTableViewCellSelectionDelegate,RIImageGenerateDelegate>
@property(nonatomic,weak)IBOutlet UITableView *tableListView;
@property(nonatomic,weak)IBOutlet AssetsGroupsTableViewCell *tempCell;
@end

@implementation VPVideoListViewController_iphone
@synthesize tempCell;
@synthesize tableListView;
- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if (self)
	{
	}
	
	return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated
{
}
- (IBAction)onCamera:(id)inSender {
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
        pickerController.delegate = self;
        pickerController.videoQuality = UIImagePickerControllerQualityType640x480;
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        pickerController.showsCameraControls = YES;
        pickerController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        pickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
        pickerController.videoMaximumDuration = 20.0f;
        [self presentModalViewController:pickerController animated:YES];
    } else {
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"sophie" ofType:@"mov"];
        NSURL *videoURL = [NSURL fileURLWithPath:filePath];

        if([RIUtilities isIphoneDevice]) {
            VPFrameListViewController_iphone *frameListController = [[VPFrameListViewController_iphone alloc] init];
            frameListController.videoURL = videoURL;
            [self.navigationController pushViewController:frameListController animated:YES];
        } else {
            VPFrameListViewController_iphone *frameListController = [[VPFrameListViewController_iphone alloc] init];
            frameListController.videoURL = videoURL;
            [self.navigationController pushViewController:frameListController animated:YES];
            
        }
//        VPFrameExtractController *frameExtractor = [[VPFrameExtractController alloc] initWithVideoURL:videoURL];
//        [frameExtractor startFrameExtraction];
//        return;
    }
}
// MARK: UIImagePickerControllerDelegates
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissModalViewControllerAnimated:NO];
    if([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.movie"] ) {
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        if([RIUtilities isIphoneDevice]) {
            VPFrameListViewController_iphone *frameListController = [[VPFrameListViewController_iphone alloc] init];
            frameListController.videoURL = videoURL;
            [self.navigationController pushViewController:frameListController animated:YES];
        }

    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Video-Camera-Icon.png"]
                                                                              style:UIBarButtonItemStyleBordered
                                                                             target:self action:@selector(onCamera:)];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 170.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return ceil((float)self.videoLibrary.assetItems.count / 2);
}
- (void)configureCell:(UITableViewCell *)cell forVideo:(AssetItem *)inVideo videoIndex:(NSInteger)inVideoIndex
{
//    if([[NSFileManager defaultManager] fileExistsAtPath:[inVideo.assetURL path]]) {
//        AVURLAsset *videoAsset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:[inVideo.assetURL path]] options:nil];
        RIImageGenerateHandler *imageGenerator = [[RIImageGenerateHandler alloc] initWithVideoAsset:inVideo.videoAsset];
        imageGenerator.delegate = self;
        AssetsGroupsTableViewCell *myThumbCell = (AssetsGroupsTableViewCell *)cell;
        [imageGenerator grabFirstImageFrameForRowIndex:myThumbCell.rowNumber withVideoIndex:inVideoIndex];
//    }
}
- (void)handler:(RIImageGenerateHandler *)inHandler didGenerateImage:(UIImage *)inImage
    forRowIndex:(NSInteger)inIndex
 withVideoIndex:(NSInteger)inVideoIndex {
    
    AssetsGroupsTableViewCell *cell = (AssetsGroupsTableViewCell *) [self.tableListView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:inIndex inSection:0]];
    
    if(inVideoIndex) {
        [cell setRightPosterImage:inImage];
//        [cell setRightSpinningActivity:NO];
    } else {
        [cell setLeftPosterImage:inImage];
//        [cell setLeftSpinningActivity:NO];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    AssetsGroupsTableViewCell *cell = (AssetsGroupsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"AssetsGroupsTableViewCell" owner:self options:nil];
        cell = tempCell;
        tempCell = nil;
    }
    
    cell.rowNumber = indexPath.row;
    cell.selectionDelegate = self;
    
    // Configure the cell.
    NSUInteger leftGroupIndex = indexPath.row * 2;
    NSUInteger rightGroupIndex = leftGroupIndex + 1;
    
    AssetItem *leftGroup = [self.videoLibrary.assetItems objectAtIndex:leftGroupIndex];
    AssetItem *rightGroup = (rightGroupIndex < self.videoLibrary.assetItems.count) ? [self.videoLibrary.assetItems objectAtIndex:rightGroupIndex] : nil;
    
    if (leftGroup) {
        
        [self configureCell:cell forVideo:leftGroup videoIndex:0];
//        [cell setLeftLabelText:@"Bhatt"];
        [leftGroup loadTitleWithCompletionHandler:^{
            AssetsGroupsTableViewCell *thumbnailCell = (AssetsGroupsTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            [thumbnailCell setLeftLabelText:leftGroup.title];
            [thumbnailCell setNeedsLayout];
        }];
        
    }
    if (rightGroup) {
        [self configureCell:cell forVideo:rightGroup videoIndex:1];
//        [cell setRightLabelText:@"Ashish"];
        [rightGroup loadTitleWithCompletionHandler:^{
            AssetsGroupsTableViewCell *thumbnailCell = (AssetsGroupsTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            [thumbnailCell setLeftLabelText:rightGroup.title];
            [thumbnailCell setNeedsLayout];
        }];
        [cell rightPosterImageView].userInteractionEnabled = YES;
    } else {
        [cell setRightLabelText:@""];
        [cell setRightPosterImage:nil];
        [cell rightPosterImageView].userInteractionEnabled = NO;
    }

    return cell;
//	AssetItem *asset = nil;
    /*
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     if (cell == nil)
     {
     cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
     cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
     }
     
     asset = [self.videoLibrary.assetItems objectAtIndex:indexPath.row];
     
     // Set and load the movie title if available
     cell.textLabel.text = [asset loadTitleWithCompletionHandler:^{
     UITableViewCell *thumbnailCell = [tableView cellForRowAtIndexPath:indexPath];
     thumbnailCell.textLabel.text = asset.title;
     [thumbnailCell setNeedsLayout];
     }];
     
     // Set and load the movie thumbnail if available
     cell.imageView.image = [asset loadThumbnailWithCompletionHandler:^{
     UITableViewCell *thumbnailCell = [tableView cellForRowAtIndexPath:indexPath];
     thumbnailCell.imageView.image = asset.thumbnail;
     [thumbnailCell setNeedsLayout];
     }];
     
     return cell;
     */ 
}
#pragma mark -
#pragma mark AssetsGroupsTableViewCellSelectionDelegate
- (void)assetsGroupsTableViewCell:(AssetsGroupsTableViewCell *)cell selectedGroupAtIndex:(NSUInteger)index {
    
    AssetItem* videoItem = [self.videoLibrary.assetItems objectAtIndex:(cell.rowNumber * 2) + index];
    
    VPFrameListViewController_iphone *frameListController = [[VPFrameListViewController_iphone alloc] init];
    frameListController.videoURL = videoItem.assetURL;
//    NSLog(@"DATA : %@",[videoItem.videoAsset commonMetadata]);
    [self.navigationController pushViewController:frameListController animated:YES];
    
}
@end
