//
//  VPRootViewController_ipad.m
//  Video2Photos
//
//  Created by Ashish Bhatt on 20/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VPRootViewController_ipad.h"

@interface VPRootViewController_ipad ()
@property (nonatomic, weak) IBOutlet iCarousel * __weak carousel;
@property(nonatomic,strong)VPImageGenerateHandler *imageGenerator;
- (VPImageGenerateHandler *)imageGeneratorHandler;
@end

@implementation VPRootViewController_ipad
@synthesize imageGenerator;
@synthesize carousel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (VPImageGenerateHandler *)imageGeneratorHandler {
    if(!self.imageGenerator) {
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"Sydney-iPhone" ofType:@"m4v"];
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:videoPath] options:nil];
        self.imageGenerator = [[VPImageGenerateHandler alloc] initWithAsset:asset];
        self.imageGenerator.delegate = self;
    }
    return self.imageGenerator;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    carousel.type = iCarouselTypeWheel;
//    carousel.vertical = YES;
    carousel.bounceDistance = 5.0f;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}
#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //generate 100 buttons
    //normally we'd use a backing array
    //as shown in the basic iOS example
    //but for this example we haven't bothered
    return [self imageGeneratorHandler].totalNumberOfFrames;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{ 
	UIButton *button = (UIButton *)view;
	if (button == nil)
	{
		//no button available to recycle, so create new one
		UIImage *image = [UIImage imageNamed:@"page.png"];
		button = [UIButton buttonWithType:UIButtonTypeCustom];
		button.frame = CGRectMake(0.0f, 0.0f, image.size.width, image.size.height);
		[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
		[button setBackgroundImage:image forState:UIControlStateNormal];
		button.titleLabel.font = [button.titleLabel.font fontWithSize:50];
        button.layer.borderColor = [UIColor whiteColor].CGColor;
        button.layer.borderWidth = 4.0f;
        button.layer.cornerRadius = 8.0f;
		[button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
	}
    [[self imageGeneratorHandler] generateImageForIndex:index];	
	//set button label
	[button setTitle:[NSString stringWithFormat:@"%i", index] forState:UIControlStateNormal];
	
	return button;
}

#pragma mark -
#pragma mark Button tap event

- (void)buttonTapped:(UIButton *)sender
{
	//get item index for button
//	NSInteger index = [carousel indexOfItemViewOrSubview:sender];
	
//    [[[[UIAlertView alloc] initWithTitle:@"Button Tapped"
//                                 message:[NSString stringWithFormat:@"You tapped button number %i", index]
//                                delegate:nil
//                       cancelButtonTitle:@"OK"
//                       otherButtonTitles:nil] autorelease] show];
}
#pragma mark -
#pragma mark Image generator delegate
- (void)handler:(VPImageGenerateHandler *)inHandler didFindImage:(UIImage *)inImage forIndex:(NSInteger)inIndex {
    UIButton *button = (UIButton *) [carousel itemViewAtIndex:inIndex];
    [button setBackgroundImage:inImage forState:UIControlStateNormal];
    [carousel setNeedsDisplay];
}
@end
