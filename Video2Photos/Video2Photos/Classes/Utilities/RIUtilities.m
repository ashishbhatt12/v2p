//
//  RIUtilities.m
//  ReverseIt
//
//  Created by Ashish Bhatt on 8/23/12.
//  Copyright (c) 2012 Capgemini. All rights reserved.
//

#import "RIUtilities.h"
#import <AssetsLibrary/AssetsLibrary.h>
#define EXTNSN @"png"
@implementation RIUtilities
+ (BOOL)isIOS6 {
    return ([[UIDevice currentDevice].systemVersion isEqualToString:@"6.0"]);
}

+ (BOOL)isIphoneDevice {
    return ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone);
}
+ (NSString *)appDocumentsDirectory {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return documentsDirectory;
}
+ (BOOL)removeMainVideosDirectory {
    NSString *directoryPath = [[RIUtilities appDocumentsDirectory] stringByAppendingPathComponent:@"VIDEOS"];
    if([[NSFileManager defaultManager] fileExistsAtPath:directoryPath]) {
        return [[NSFileManager defaultManager] removeItemAtPath:directoryPath error:nil];
    } else {
        return YES;
    }
        
}
+ (NSString *)videoExtractDirectoryForVideo:(NSInteger)inVideoID {
    NSString *directoryPath = [[RIUtilities appDocumentsDirectory] stringByAppendingPathComponent:[NSString stringWithFormat:@"VIDEOS/%d/VIDEO-FRAMES",inVideoID]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:directoryPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
    return directoryPath;
}

+ (NSString *)videoExtractDirectory {
    NSString *directoryPath = [[RIUtilities appDocumentsDirectory] stringByAppendingPathComponent:@"VIDEOS/VIDEO-FRAMES"];
    if(![[NSFileManager defaultManager] fileExistsAtPath:directoryPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
    return directoryPath;
}
+ (NSString *)outputFilePathForVideoWithLastFrameIndex:(NSInteger)inLastFrameIndex forVideo:(NSInteger)inVideoIndex {
    NSString *filePath = [[RIUtilities appDocumentsDirectory] stringByAppendingPathComponent:
                          [NSString stringWithFormat:@"VIDEOS/%d/MERGED-FRAMES/MERGED-%d.%@",inVideoIndex, inLastFrameIndex,EXTNSN]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:[filePath stringByDeletingLastPathComponent]]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:[filePath stringByDeletingLastPathComponent]
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
    return filePath;
}
+ (NSString *)pathForDirectoryForVideo:(NSInteger)inVideID {
    NSString *directoryPath = [[RIUtilities appDocumentsDirectory]
                               stringByAppendingPathComponent:[NSString stringWithFormat:@"VIDEOS/%d",inVideID]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:directoryPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
    return directoryPath;
}
+ (NSString *)outputImageFramePathIndex:(NSInteger)inLastFrameIndex {
    NSString *directoryPath = [[RIUtilities appDocumentsDirectory] stringByAppendingPathComponent:@"VIDEOS/IMAGE-FRAMES"];
    if(![[NSFileManager defaultManager] fileExistsAtPath:directoryPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
    return [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.png",inLastFrameIndex]];
}
+ (NSString *)filePathForFrameIndex:(NSInteger)inIndex forVideo:(NSInteger)inVideoIndex {
    NSString *framePath = [[RIUtilities videoExtractDirectoryForVideo:inVideoIndex] stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.%@",inIndex,EXTNSN]];
    return framePath;
    if([[NSFileManager defaultManager] fileExistsAtPath:framePath]) {
        return framePath;
    } else {
        return nil;
    }
}
+ (AVAsset *)assetForFilePath:(NSString *)inFilePath {
    NSURL *filePathURL = [[NSURL alloc] initFileURLWithPath:inFilePath];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:filePathURL options:nil];
    return asset;
}
+ (UIInterfaceOrientation)orientationForTrack:(AVAsset *)asset {
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    if (size.width == txf.tx && size.height == txf.ty)
        return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == 0)
        return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == size.width)
        return UIInterfaceOrientationPortraitUpsideDown;
    else
        return UIInterfaceOrientationPortrait;
}
+ (NSString *)videoPresetForQuality:(VIDEOQUALITY)inQuality {
    NSString *videoPrset = AVAssetExportPresetMediumQuality;
    switch (inQuality) {
        case eLowQuality:
            videoPrset = AVAssetExportPresetLowQuality;
            break;
        case eMediumQuality:
            videoPrset = AVAssetExportPresetMediumQuality;
            break;
        case eHighQuality:
            videoPrset = AVAssetExportPreset640x480;//AVAssetExportPresetHighestQuality;
            break;
        default:
            videoPrset = AVAssetExportPreset640x480;//AVAssetExportPresetHighestQuality;            
            break;
    }
    return videoPrset;
}
+ (UIImagePickerControllerQualityType )cameraVideoQuallity:(VIDEOQUALITY)inQuality {
    UIImagePickerControllerQualityType cameraQuality = UIImagePickerControllerQualityTypeLow;
    switch (inQuality) {
        case eLowQuality:
            cameraQuality = UIImagePickerControllerQualityTypeLow;
            break;
        case eMediumQuality:
            cameraQuality = UIImagePickerControllerQualityTypeMedium;
            break;
        case eHighQuality:
            cameraQuality = UIImagePickerControllerQualityType640x480;//UIImagePickerControllerQualityTypeHigh;
            break;
        default:
            cameraQuality = UIImagePickerControllerQualityTypeLow;            
            break;
    }
    return cameraQuality;
}
+ (NSInteger)framesPerSeconds:(FPS)inFPS {
    NSInteger framesPerSeconds = 0;
    switch (inFPS) {
        case e10FPS:
            framesPerSeconds = 10;
            break;
        case e20FPS:
            framesPerSeconds = 20;
            break;
        case e30FPS:
            framesPerSeconds = 30;
            break;
        default:
            framesPerSeconds = 10;            
            break;
    }
    return framesPerSeconds;
}
+ (NSString *)framesOutputPathForVideo:(NSString *)inVideoName {
    NSString *directoryPath = [[RIUtilities appDocumentsDirectory] stringByAppendingPathComponent:inVideoName];
    return directoryPath;
}
+ (NSString *)framePathAtIndex:(NSInteger)inIndex forFolder:(NSString *)inFolder {
    NSString *directoryPath = [RIUtilities framesOutputPathForVideo:inFolder];
    if(![[NSFileManager defaultManager] fileExistsAtPath:directoryPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
    return [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.png",inIndex]];    
}
+ (NSString *)outputImageFrameIndex:(NSInteger)inIndex {
    NSString *directoryPath = [[RIUtilities appDocumentsDirectory] stringByAppendingPathComponent:@"FRAMES"];
    if(![[NSFileManager defaultManager] fileExistsAtPath:directoryPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
    return [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.png",inIndex]];
}
+ (NSString *)appIntroSongFilePath {
    return [[NSBundle mainBundle] pathForResource:@"App_Intro_Song" ofType:@"wav"];
}
+ (NSURL *)copyVideo:(NSURL *)inURL {
    __block NSURL *outputURL = nil;
    @autoreleasepool {
        ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
        [assetLibrary assetForURL:inURL resultBlock:^(ALAsset *asset) {
            ALAssetRepresentation *rep = [asset defaultRepresentation];
            Byte *buffer = (Byte*)malloc(rep.size);
            NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
            NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
            NSString *videoPath = [[RIUtilities appDocumentsDirectory] stringByAppendingPathComponent:@"SampleVideo.mov"];
            if([[NSFileManager defaultManager] fileExistsAtPath:videoPath]) {
                [[NSFileManager defaultManager] removeItemAtPath:videoPath error:nil];
            }
            [[NSFileManager defaultManager] createFileAtPath:videoPath contents:data attributes:nil];
            outputURL = [NSURL fileURLWithPath:videoPath];
//            [self performSelectorOnMainThread:@selector(extractVideoFrames) withObject:nil waitUntilDone:YES];
        } failureBlock:^(NSError *err) {
            NSLog(@"Error: %@",[err localizedDescription]);
        }];
    }
    return outputURL;
}
+ (BOOL)isLandscapeVideo:(NSURL *)inVideoURL
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:inVideoURL options:nil];
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    UIInterfaceOrientation orientation = UIInterfaceOrientationPortrait;
    
    if (size.width == txf.tx && size.height == txf.ty)
        orientation = UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == 0)
        orientation = UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == size.width)
        orientation = UIInterfaceOrientationPortraitUpsideDown;
    else
        orientation = UIInterfaceOrientationPortrait;
    
    if(UIInterfaceOrientationIsLandscape(orientation)) {
        return YES;
    } else {
        return NO;
    }
}
@end
