//
//  VPFrameSaveOperation.m
//  Video2Photos
//
//  Created by Ashish on 30/09/12.
//
//

#import "VPFrameSaveOperation.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "RIUtilities.h"

@implementation VPFrameSaveOperation
@synthesize delegate;
@synthesize frameIndex;
@synthesize folderName;
- (void)main {
    @autoreleasepool {
        
//    NSString *photoFilePath = [RIUtilities outputImageFrameIndex:self.frameIndex];
    NSString *photoFilePath = [RIUtilities framePathAtIndex:self.frameIndex forFolder:self.folderName];
    UIImage *frameImage = [[UIImage alloc] initWithContentsOfFile:photoFilePath];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.2]];
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeImageToSavedPhotosAlbum:[frameImage CGImage]
                              orientation:(ALAssetOrientation)[frameImage imageOrientation]
                          completionBlock:^(NSURL *assetURL, NSError *error){
                              if (error) {
                                  // TODO: error handling
                              } else {
                                  // TODO: success handling
                                  [self.delegate operation:self didFinishSavingFrameAtIndex:self.frameIndex];
                              }
                          }];
    }
}
@end
