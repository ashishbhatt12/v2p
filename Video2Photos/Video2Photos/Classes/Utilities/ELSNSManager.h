//
//  ELSNSManager.h
//  ELMO
//
//  Created by Ashish Bhatt on 27/07/12.
//  Copyright (c) 2012 GlobalLogic Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>
typedef enum _ELSNSType {
    eFacebook,
    eTwitter,
    eMixi,
}ELSNSType;
@interface ELSNSManager : NSObject

// Gives the currently login accounts for Twitter
- (NSArray *)enabledAccountsForTwitter;

// Updates the status for the mentionded SNS type with passed parameters
- (BOOL)updateStatusForService:(ELSNSType)inService withParameters:(NSDictionary *)inParameters;

// Presents the status update sheet to user with giver parameters on passed controller
- (BOOL)presentStatusUpdateSheetWithParameters:(NSDictionary *)inParameters forService:(ELSNSType)inType onController:(UIViewController *)inController;

// Presents the status update sheet to post the status to Facebook.
// #NOTE: This can used only when iOS6 support is available
- (BOOL)presentFacebookSheetWithParameters:(NSDictionary *)inParameters onController:(UIViewController *)inController;

// Presents the Tweet sheet to post the status to Twitter.
- (BOOL)presentTweetSheetWithParameters:(NSDictionary *)inParameters onController:(UIViewController *)inController;

// Updates the staus on Facebook
- (BOOL)updateFacebookStatus:(NSDictionary *)inParameters;

// Updates the staus on Facebook
- (BOOL)updateTwitterStatus:(NSDictionary *)inParameters;

// Updates the staus on Mixi
- (BOOL)updateMixiStatus:(NSDictionary *)inParameters;

@end

@interface ELSNSManager(Private)
// Returns the first account from the registred Twitter accounts
- (ACAccount *)firstTwitterAcount;
@end
