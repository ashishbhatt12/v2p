//
//  RIConstants.h
//  ReverseIt
//
//  Created by Ashish Bhatt on 9/4/12.
//  Copyright (c) 2012 Capgemini. All rights reserved.
//

#ifndef ReverseIt_RIConstants_h
#define ReverseIt_RIConstants_h

#define OPTIMIZATION 1

#define FRAMES_TO_CAPTURE   10

#define kVideoQuality @"VIDEO_QUALITY"
#define kVideoFPS @"VIDEO_FPS"
typedef enum _VIDEOQUALITY {
    eLowQuality,
    eMediumQuality,
    eHighQuality,
}VIDEOQUALITY;

typedef enum _FPS {
    e10FPS,
    e20FPS,
    e30FPS,
}FPS;
#endif
