//
//  ELSNSManager.m
//  ELMO
//
//  Created by Ashish Bhatt on 27/07/12.
//  Copyright (c) 2012 GlobalLogic Inc. All rights reserved.
//

#import "ELSNSManager.h"
#import "RIUtilities.h"
#import <Social/Social.h>

@implementation ELSNSManager
- (NSArray *)enabledAccountsForTwitter {
    
    __block NSArray *the_pTwitterAccounts = nil;
    
    //  First, we need to obtain the account instance for the user's Twitter account
    ACAccountStore *the_pStore = [[ACAccountStore alloc] init];
    ACAccountType *the_pTwitterAccountType = [the_pStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    //  Request permission from the user to access the available Twitter accounts
    [the_pStore requestAccessToAccountsWithType:the_pTwitterAccountType withCompletionHandler:
     ^(BOOL the_bGranted, NSError *the_pError) {
         
         if (!the_bGranted) {
             // The user rejected your request 
             NSLog(@"User rejected access to the account.");
         } else {
             // Grab the available accounts
             the_pTwitterAccounts = [the_pStore accountsWithAccountType:the_pTwitterAccountType];
         }
     }];
    
//    [the_pStore release];
    return the_pTwitterAccounts;
}
- (ACAccount *)firstTwitterAcount {
    ACAccount *the_pFirstAccount = nil;
    NSArray *the_pTwitterAccounts = [self enabledAccountsForTwitter];
    if([the_pTwitterAccounts count]) {
        the_pFirstAccount = [the_pTwitterAccounts objectAtIndex:0];
    }
    return the_pFirstAccount;
}
#pragma mark - status updates methods 
- (BOOL)updateStatusForService:(ELSNSType)inService withParameters:(NSDictionary *)inParameters {
    return NO;
}

- (BOOL)presentStatusUpdateSheetWithParameters:(NSDictionary *)inParameters forService:(ELSNSType)inType onController:(UIViewController *)inController {
    switch (inType) {
        case eTwitter:
            return [self presentTweetSheetWithParameters:inParameters onController:inController];
            break;
        case eFacebook:
            [self presentFacebookSheetWithParameters:inParameters onController:inController];
                        
            break;
        case eMixi:
            break;
        default:
            break;
    }
    return NO;
}
- (BOOL)presentFacebookSheetWithParameters:(NSDictionary *)inParameters onController:(UIViewController *)inController {
    if([RIUtilities isIOS6]) {
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
            SLComposeViewController *facebookComposeController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            
            facebookComposeController.completionHandler = ^(SLComposeViewControllerResult the_iResult) {
                switch(the_iResult) {
                    case SLComposeViewControllerResultCancelled:
                        //  This means the user cancelled without sending the Tweet
                        break;
                    case SLComposeViewControllerResultDone:
                        //  This means the user hit 'Send'
                        break;
                }
                
                //  dismiss the Tweet Sheet
                dispatch_async(dispatch_get_main_queue(), ^{
                    [inController dismissViewControllerAnimated:YES completion:^{
                        NSLog(@"Facebook Sheet has been dismissed.");
                    }];
                });
            };
            
            
            UIImage *attachedImage = [inParameters objectForKey:@"IMAGE"];
            if (![facebookComposeController addImage:attachedImage]) {
                NSLog(@"Unable to add the image!");
            }
            
            [facebookComposeController setInitialText:@"Check out this image frame!"];

            //  Add an URL to the Tweet.  You can add multiple URLs.
            //    if (![the_pTweetSheetController addURL:[NSURL URLWithString:@"http://twitter.com/"]]){
            //        NSLog(@"Unable to add the URL!");
            //    }
            
            
            //  Presents the Tweet Sheet to the user
            [inController presentViewController:facebookComposeController animated:YES completion:^{
                NSLog(@"Facebook sheet has been presented.");
            }];    

            
        }
    } else {
        // implement facebook sdk
    }

    return YES;
}

- (BOOL)presentTweetSheetWithParameters:(NSDictionary *)inParameters onController:(UIViewController *)inController {
    //  Create an instance of the Tweet Sheet
    TWTweetComposeViewController *the_pTweetSheetController = [[TWTweetComposeViewController alloc] init];
    
    // Sets the completion handler.  Note that we don't know which thread the
    // block will be called on, so we need to ensure that any UI updates occur
    // on the main queue
    the_pTweetSheetController.completionHandler = ^(TWTweetComposeViewControllerResult the_iResult) {
        switch(the_iResult) {
            case TWTweetComposeViewControllerResultCancelled:
                //  This means the user cancelled without sending the Tweet
                break;
            case TWTweetComposeViewControllerResultDone:
                //  This means the user hit 'Send'
                break;
        }
        
        //  dismiss the Tweet Sheet 
        dispatch_async(dispatch_get_main_queue(), ^{            
            [inController dismissViewControllerAnimated:YES completion:^{
                NSLog(@"Tweet Sheet has been dismissed."); 
            }];
        });
    };
    
    //  Set the initial body of the Tweet
    [the_pTweetSheetController setInitialText:@"Check out this image frame!"];
    
    //  Adds an image to the Tweet
    //  For demo purposes, assume we have an image named 'larry.png'
    //  that we wish to attach
    
    UIImage *attachedImage = [inParameters objectForKey:@"IMAGE"];
    if (![the_pTweetSheetController addImage:attachedImage]) {
        NSLog(@"Unable to add the image!");
    }
    
    //  Add an URL to the Tweet.  You can add multiple URLs.
//    if (![the_pTweetSheetController addURL:[NSURL URLWithString:@"http://twitter.com/"]]){
//        NSLog(@"Unable to add the URL!");
//    }
    
    
    //  Presents the Tweet Sheet to the user
    [inController presentViewController:the_pTweetSheetController animated:YES completion:^{
        NSLog(@"Tweet sheet has been presented.");
    }];    
    
//    [the_pTweetSheetController release];
    return YES;
}

- (BOOL)updateFacebookStatus:(NSDictionary *)inParameters {
    return NO;
}

- (BOOL)updateTwitterStatus:(NSDictionary *)inParameters {
    return NO;
}

- (BOOL)updateMixiStatus:(NSDictionary *)inParameters {
    return NO;
}

@end
