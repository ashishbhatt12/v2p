//
//  RIUtilities.h
//  ReverseIt
//
//  Created by Ashish Bhatt on 8/23/12.
//  Copyright (c) 2012 Capgemini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
@interface RIUtilities : NSObject
+ (BOOL)isIOS6;
+ (BOOL)isIphoneDevice;
+ (BOOL)removeMainVideosDirectory;
+ (NSString *)appDocumentsDirectory;
+ (NSString *)videoExtractDirectory;
+ (NSString *)outputFilePathForVideoWithLastFrameIndex:(NSInteger)inLastFrameIndex forVideo:(NSInteger)inVideoIndex;
+ (NSString *)filePathForFrameIndex:(NSInteger)inIndex forVideo:(NSInteger)inVideoIndex;
+ (UIInterfaceOrientation)orientationForTrack:(AVAsset *)asset;
+ (AVAsset *)assetForFilePath:(NSString *)inFilePath;
+ (NSString *)outputImageFramePathIndex:(NSInteger)inLastFrameIndex;
+ (NSString *)videoPresetForQuality:(VIDEOQUALITY)inQuality;
+ (UIImagePickerControllerQualityType )cameraVideoQuallity:(VIDEOQUALITY)inQuality;
+ (NSString *)videoExtractDirectoryForVideo:(NSInteger)inVideoID;
+ (NSInteger)framesPerSeconds:(FPS)inFPS;
+ (NSString *)pathForDirectoryForVideo:(NSInteger)inVideID;
+ (NSString *)outputImageFrameIndex:(NSInteger)inIndex;
+ (NSString *)appIntroSongFilePath;
+ (NSURL *)copyVideoToApplication;
+ (BOOL)isLandscapeVideo:(NSURL *)inVideoURL;
+ (NSString *)framesOutputPathForVideo:(NSString *)inVideoName;
+ (NSString *)framePathAtIndex:(NSInteger)inIndex forFolder:(NSString *)inFolder;
@end
