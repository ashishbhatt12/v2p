//
//  VPFrameSaveOperation.h
//  Video2Photos
//
//  Created by Ashish on 30/09/12.
//
//

#import <Foundation/Foundation.h>
@protocol VPFrameSaveDelegate;
@interface VPFrameSaveOperation : NSOperation
@property(nonatomic,weak)NSObject<VPFrameSaveDelegate> *__weak delegate;
@property(nonatomic,assign)NSInteger frameIndex;
@property(nonatomic,strong)NSString *folderName;
@end

@protocol VPFrameSaveDelegate
- (void)operation:(VPFrameSaveOperation *)inOperation didFinishSavingFrameAtIndex:(NSInteger)inIndex;
@end