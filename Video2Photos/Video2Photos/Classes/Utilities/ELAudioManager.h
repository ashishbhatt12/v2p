//
//  ELAudioManager.h
//  ELMO
//
//  Created by Ashish Bhatt on 20/07/12.
//  Copyright (c) 2012 GlobalLogic Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
@interface ELAudioManager : NSObject {
    @private
    AVAudioPlayer *m_pAudioPlayer;    
}
@property(nonatomic,strong)AVAudioPlayer *audioPlayer;
+ (ELAudioManager *)sharedManager;
#pragma mark - Playing Audio
- (void)playAudio:(NSString *)inFilePath;
- (void)stopPlaying;
- (void)playAppIntroductionSong;
@end
