//
//  ELAudioManager.m
//  ELMO
//
//  Created by Ashish Bhatt on 20/07/12.
//  Copyright (c) 2012 GlobalLogic Inc. All rights reserved.
//

#import "ELAudioManager.h"
#import "RIUtilities.h"


@implementation ELAudioManager
@synthesize audioPlayer = m_pAudioPlayer;
+ (ELAudioManager *)sharedManager {
    static ELAudioManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    
    return sharedMyManager;
}
- (id)init {
    if (self = [super init]) {
    }
    return self;
}
#pragma mark - Playing Audio
- (void)playAppIntroductionSong {
    return;
    NSString *the_pRecordingFilePath = [RIUtilities appIntroSongFilePath];
    [self playAudio:the_pRecordingFilePath];
}
- (void)playAudio:(NSString *)inFilePath {
    AVAudioSession *the_pAudioSession = [AVAudioSession sharedInstance];
    [the_pAudioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    NSURL *the_pPathUrl = [NSURL fileURLWithPath:inFilePath];
    
    NSError *the_pError;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:the_pPathUrl error:&the_pError];
    self.audioPlayer.numberOfLoops = 0;
    [self.audioPlayer play];
    
    [NSTimer scheduledTimerWithTimeInterval:30.0f target:self selector:@selector(stopPlaying) userInfo:nil repeats:NO];
}
- (void)stopPlaying {
    [self.audioPlayer stop];
    NSLog(@"playing stopped");
}
@end
