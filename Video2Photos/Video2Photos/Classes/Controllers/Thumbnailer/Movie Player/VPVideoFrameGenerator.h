//
//  VPVideoFrameGenerator.h
//  Video2Photos
//
//  Created by Ashish on 16/12/12.
//
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "FrameWriteOperation.h"
#import "VPFrameCaptureOperation.h"

@protocol VPVideoFrameGenerateDelegate;
@interface VPVideoFrameGenerator : NSObject <FrameWriteDelegate,VPFrameCaptureDelegate>

@property(nonatomic,strong)MPMoviePlayerController *moviePlayer;
@property(nonatomic,strong)NSTimer *thumbnailTimer;
@property(nonatomic,assign)double thumbnailTime;
@property(nonatomic,assign)NSInteger thumbnailCounter;
@property(nonatomic,assign)NSInteger thumbnailIndex;
@property(nonatomic,assign)NSInteger numberOfThumbsNeeded;
@property(nonatomic,assign)NSInteger completedFramesCount;
@property(nonatomic,strong)NSString *folderName;
@property(nonatomic,strong)NSOperationQueue *frameOperationQueue;
@property(nonatomic,weak)NSObject<VPVideoFrameGenerateDelegate> * __weak delegate;
@property(nonatomic,strong)NSURL *movieURL;
- (id)initWithVideo:(NSURL *)inURL;
- (void)initiateThumbnailFramingAtIndex:(NSInteger)inIndex;
- (void)endThumbnailFraming;
@end

@protocol VPVideoFrameGenerateDelegate
- (void)didStartFramingWithGenerator:(VPVideoFrameGenerator *)inGenerator;
- (void)generator:(VPVideoFrameGenerator *)inGenerator didGenerateFrame:(UIImage *)inFrame forIndex:(NSInteger)inIndex finishedPercent:(CGFloat)inPercent;
- (void)didFinishFramingWithGenerator:(VPVideoFrameGenerator *)inGenerator;
@end
