//
//  VPVideoFrameGenerator.m
//  Video2Photos
//
//  Created by Ashish on 16/12/12.
//
//

#import "VPVideoFrameGenerator.h"
#import "RIUtilities.h"
#import <SDWebImage/SDImageCache.h>

#define FRAMES_PER_SECONDS  24.0f

@implementation VPVideoFrameGenerator
@synthesize moviePlayer = _moviePlayer;
@synthesize thumbnailTimer = _thumbnailTimer;
@synthesize thumbnailTime = _thumbnailTime;
@synthesize thumbnailCounter = _thumbnailCounter;
@synthesize numberOfThumbsNeeded = _numberOfThumbsNeeded;
@synthesize folderName = _folderName;
@synthesize frameOperationQueue = _frameOperationQueue;
@synthesize delegate = _delegate;
@synthesize thumbnailIndex = _thumbnailIndex;
@synthesize completedFramesCount = _completedFramesCount;
@synthesize movieURL = _movieURL;
- (id)initWithVideo:(NSURL *)inURL {
    if(self = [super init]) {
        self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:inURL];
        self.moviePlayer.shouldAutoplay = NO;
        [self.moviePlayer prepareToPlay];        
        self.movieURL = inURL;
        self.frameOperationQueue = [[NSOperationQueue alloc] init];
//        self.frameOperationQueue.maxConcurrentOperationCount = 5;
    }
    return self;
}
- (void)initiateThumbnailFramingAtIndex:(NSInteger)inIndex {

    [self.thumbnailTimer invalidate];
    self.thumbnailTimer = nil;
    
//    [self.frameOperationQueue setSuspended:YES];
    [self.frameOperationQueue cancelAllOperations];
    
    [self.delegate didStartFramingWithGenerator:self];
    
    self.thumbnailCounter = 0;
    self.completedFramesCount = 0;
    
    self.thumbnailTime = inIndex * (1.0f / FRAMES_PER_SECONDS);
    self.thumbnailIndex = inIndex;
    
    
    self.thumbnailTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f / FRAMES_PER_SECONDS
                                                           target:self
                                                         selector:@selector(takeThumbnail:)
                                                         userInfo:nil
                                                          repeats:YES];
}
- (void)endThumbnailFraming {
    NSLog(@"End thubnailing");
    [self.thumbnailTimer invalidate];
    self.thumbnailTimer = nil;
}
- (void)takeThumbnail:(NSTimer *)inTimer {
    if(self.numberOfThumbsNeeded <= self.thumbnailCounter) {
        [self endThumbnailFraming];
    }

//    NSString *frameFilePath = [RIUtilities framePathAtIndex:self.thumbnailIndex forFolder:self.folderName];
//    if(![[NSFileManager defaultManager] fileExistsAtPath:frameFilePath]) {
    UIImage *cachedImage = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:[NSString stringWithFormat:@"%d",self.thumbnailIndex]];
    if(!cachedImage) {
        [self addFrameCaptureOperationForTime:self.thumbnailTime withIndex:self.thumbnailIndex];
    } else {
        [self.delegate generator:self didGenerateFrame:cachedImage forIndex:self.thumbnailIndex finishedPercent:1];
    }
//    NSString *key = [NSString stringWithFormat:@"%d",self.thumbnailIndex];
//    if(![[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:key]) {
//            UIImage *thumbNail = [self.moviePlayer thumbnailImageAtTime:self.thumbnailTime timeOption:MPMovieTimeOptionExact];
//            [[SDImageCache sharedImageCache] storeImage:thumbNail forKey:key];
//            [self.delegate generator:self didGenerateFrame:thumbNail forIndex:self.thumbnailIndex finishedPercent:0];
//    }
    //        [self addFrame:thumbNail atIndex:self.thumbnailIndex];
//    }
    
    
    self.thumbnailTime += (1.0f / FRAMES_PER_SECONDS);
    self.thumbnailCounter++;
    self.thumbnailIndex++;
}
#pragma mark - Frame capture operation
- (void)addFrameCaptureOperationForTime:(double)inTime withIndex:(NSInteger)inIndex {
//    if([self.frameOperationQueue isSuspended])
//        [self.frameOperationQueue setSuspended:NO];
    VPFrameCaptureOperation *captureOperation = [[VPFrameCaptureOperation alloc] initWithFrameIndex:inIndex forTime:inTime forMovie:self.movieURL];
    captureOperation.folderName = self.folderName;
    captureOperation.delegate = self;
    [self.frameOperationQueue addOperation:captureOperation];
}
- (MPMoviePlayerController *)moviePlayerController {
    return self.moviePlayer;
}
- (void)operation:(VPFrameCaptureOperation *)inOperation didCaptureFrame:(UIImage *)inFrame forIndex:(NSInteger)inIndex {
    [self performSelectorOnMainThread:@selector(updateFrameWithInfo:)
                           withObject:[NSDictionary dictionaryWithObjectsAndKeys:inFrame,@"FRAME",[NSNumber numberWithInteger:inIndex],@"INDEX", nil]
                        waitUntilDone:YES];
}
- (void)updateFrameWithInfo:(NSDictionary *)inFrameInfo {
    UIImage *frame = [inFrameInfo objectForKey:@"FRAME"];
    NSNumber *frameIndex = [inFrameInfo objectForKey:@"INDEX"];
    [self.delegate generator:self didGenerateFrame:frame forIndex:[frameIndex integerValue] finishedPercent:1];
    [[SDImageCache sharedImageCache] storeImage:frame forKey:[NSString stringWithFormat:@"%d",[frameIndex integerValue]]];
}
#pragma mark - Framing operations
- (void)addFrame:(UIImage *)inImage atIndex:(NSInteger)inIndex {
    FrameWriteOperation *frameOperation = [[FrameWriteOperation alloc] initWithImage:inImage withInddex:inIndex];
    frameOperation.folderName = self.folderName;
    frameOperation.delegate = self;
    frameOperation.videoID = 1;
    [self.frameOperationQueue addOperation:frameOperation];
}
- (void)operation:(FrameWriteOperation *)inOperation didFinishWritingAtIndex:(NSInteger)inIndex {
    [self performSelectorOnMainThread:@selector(updateFrameWithIndex:)
                           withObject:[NSNumber numberWithInteger:inIndex]
                        waitUntilDone:YES];
}
- (void)updateFrameWithIndex:(NSNumber *)inFrameNumber {
    //    CGFloat percentage = ((CGFloat)[inFrameNumber integerValue] / (CGFloat)self.numberOfThumbsNeeded);
    //    [self.delegate generator:self didGenerateFrame:nil forIndex:[inFrameNumber integerValue] finishedPercent:percentage];
    self.completedFramesCount++;
    if(self.completedFramesCount >= self.numberOfThumbsNeeded){
        [self.delegate didFinishFramingWithGenerator:self];
    }
}

- (void)dealloc {
    self.delegate = nil;
    self.moviePlayer = nil;
    [self.frameOperationQueue cancelAllOperations];
}
@end
