//
//  VPFrameExtractController.h
//  Video2Photos
//
//  Created by HITESHKUMAR Pratapbhai Dave on 20/09/12.
//
//

#import <Foundation/Foundation.h>
#import "FrameWriteOperation.h"
//#import "VideoFrameExtractor.h"
#define FramePerSeconds 24
@protocol VPFrameExtractDelegate;
@interface VPFrameExtractController : NSObject /*<FrameWriteDelegate>*/ {
    @private
//    VideoFrameExtractor *videoFrameExtractor;
    NSURL *videoURL;
    NSOperationQueue *frameOperationQueue;
	float lastFrameTime;
    NSInteger frameIndex;
    NSInteger frameCounter;
    NSInteger framesToCompare;
    NSInteger framesPosition;
    NSObject<VPFrameExtractDelegate> *__weak delegate;
    NSTimer *framingTimer;
    BOOL landScapeVideo;
    NSString *folderName;
    BOOL framingFromScroll;
    double currentDtsValue;
}
@property(nonatomic,assign)double currentDtsValue;
@property(nonatomic,assign)BOOL framingFromScroll;
@property(nonatomic,strong)NSString *folderName;
@property(nonatomic,assign)BOOL landScapeVideo;
@property(nonatomic,assign)NSInteger framesToCompare;
@property(nonatomic,strong)NSTimer *framingTimer;
@property(nonatomic,weak)NSObject<VPFrameExtractDelegate> *__weak delegate;
@property(nonatomic,assign)NSInteger frameIndex;
@property(nonatomic,strong)NSOperationQueue *frameOperationQueue;
@property(nonatomic,strong)NSURL *videoURL;
//@property(nonatomic,strong)VideoFrameExtractor *videoFrameExtractor;
@property(nonatomic,assign)BOOL noMoreFrames;
@property(nonatomic,assign)NSInteger totalNumberFrames;
@property(nonatomic,assign)NSInteger frameCounter;
@property(nonatomic,assign)NSInteger framesPosition;
- (id)initWithVideoURL:(NSURL *)inURL;
- (void)startFrameExtractionFromIndex:(NSInteger)inIndex;
- (void)stopFramingProcess;
- (void)restartFraming;
- (NSInteger)framesToExtractForPercent:(CGFloat)inPercent;
- (void)frameAtIndex:(NSNumber *)inIndex;
- (void)configureFrameExtractor;
- (void)extractFramesAtIndex:(NSNumber *)inIndex;
@end

@protocol VPFrameExtractDelegate
@optional
- (CGSize)calculatedSizeForVideoFrames;
- (double)timeDurationSelectedVideo:(VPFrameExtractController *)inController;
- (void)controller:(VPFrameExtractController *)inController didFinishPercent:(CGFloat)inPercent;
- (void)controller:(VPFrameExtractController *)inController didGenerateFrame:(NSInteger)inIndex;
- (void)controller:(VPFrameExtractController *)inController didExtractFrameAtIndex:(NSInteger)inIndex;
- (void)didStartExtractingFramesWithController:(VPFrameExtractController *)inController;
- (void)didFinishExtractingFramesWithController:(VPFrameExtractController *)inController;
@end