//
//  VPFrameExtractController.m
//  Video2Photos
//
//  Created by HITESHKUMAR Pratapbhai Dave on 20/09/12.
//
//

#import "VPFrameExtractController.h"
#import "UIImage+Resize.h"
#import "RIUtilities.h"
static CGRect swapWidthAndHeight(CGRect rect)
{
    CGFloat swap = rect.size.width;
    
    rect.size.width = rect.size.height;
    rect.size.height = swap;
    
    return rect;
}


@interface UIImage (Rotate)
-(UIImage*)rotate:(UIImageOrientation)orient;
@end
@implementation UIImage (Rotate)
- (UIImage*)rotate:(UIImageOrientation)orient
{
    CGRect bnds = CGRectZero;
    UIImage* copy = nil;
    CGContextRef ctxt = nil;
    CGImageRef imag = self.CGImage;
    CGRect rect = CGRectZero;
    CGAffineTransform tran = CGAffineTransformIdentity;
    
    rect.size.width = CGImageGetWidth(imag);
    rect.size.height = CGImageGetHeight(imag);
    
    bnds = rect;
    
    switch (orient)
    {
        case UIImageOrientationUp:
            // would get you an exact copy of the original
            assert(false);
            return nil;
            
        case UIImageOrientationUpMirrored:
            tran = CGAffineTransformMakeTranslation(rect.size.width, 0.0);
            tran = CGAffineTransformScale(tran, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown:
            tran = CGAffineTransformMakeTranslation(rect.size.width,rect.size.height);
            tran = CGAffineTransformRotate(tran, M_PI);
            break;
            
        case UIImageOrientationDownMirrored:
            tran = CGAffineTransformMakeTranslation(0.0, rect.size.height);
            tran = CGAffineTransformScale(tran, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeft:
            bnds = swapWidthAndHeight(bnds);
            tran = CGAffineTransformMakeTranslation(0.0, rect.size.width);
            tran = CGAffineTransformRotate(tran, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeftMirrored:
            bnds = swapWidthAndHeight(bnds);
            tran = CGAffineTransformMakeTranslation(rect.size.height,rect.size.width);
            tran = CGAffineTransformScale(tran, -1.0, 1.0);
            tran = CGAffineTransformRotate(tran, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRight:
            bnds = swapWidthAndHeight(bnds);
            tran = CGAffineTransformMakeTranslation(rect.size.height, 0.0);
            tran = CGAffineTransformRotate(tran, M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored:
            bnds = swapWidthAndHeight(bnds);
            tran = CGAffineTransformMakeScale(-1.0, 1.0);
            tran = CGAffineTransformRotate(tran, M_PI / 2.0);
            break;
            
        default:
            // orientation value supplied is invalid
            assert(false);
            return nil;
    }
    
    UIGraphicsBeginImageContext(bnds.size);
    ctxt = UIGraphicsGetCurrentContext();
    
    switch (orient)
    {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextScaleCTM(ctxt, -1.0, 1.0);
            CGContextTranslateCTM(ctxt, -rect.size.height, 0.0);
            break;
            
        default:
            CGContextScaleCTM(ctxt, 1.0, -1.0);
            CGContextTranslateCTM(ctxt, 0.0, -rect.size.height);
            break;
    }
    
    CGContextConcatCTM(ctxt, tran);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), rect, imag);
    
    copy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return copy;
}


@end
#define LERP(A,B,C) ((A)*(1.0-C)+(B)*C)

@implementation VPFrameExtractController
//@synthesize videoFrameExtractor;
@synthesize videoURL;
@synthesize frameOperationQueue;
@synthesize frameIndex;
@synthesize delegate;
@synthesize framingTimer;
@synthesize noMoreFrames;
@synthesize totalNumberFrames;
@synthesize frameCounter;
@synthesize framesToCompare;
@synthesize framesPosition;
@synthesize landScapeVideo;
@synthesize folderName;
@synthesize framingFromScroll;
@synthesize currentDtsValue;
- (id)initWithVideoURL:(NSURL *)inURL {
    if(self = [super init]) {
        self.videoURL = inURL;
//        self.landScapeVideo = [RIUtilities isLandscapeVideo:self.videoURL];
    }
    return self;
}
- (void)configureFrameExtractor {
/*
    self.videoFrameExtractor = [[VideoFrameExtractor alloc] initWithVideo:[self.videoURL path]];
    self.videoFrameExtractor.dtsValue = self.currentDtsValue;
    
	// set output image size
	self.videoFrameExtractor.outputWidth = [self.delegate calculatedSizeForVideoFrames].width;// 480;
	self.videoFrameExtractor.outputHeight = [self.delegate calculatedSizeForVideoFrames].height;//320;
    
    // print some info about the video
    //	NSLog(@"video duration: %f",self.videoFrameExtractor.duration);
    //	NSLog(@"video size: %d x %d", self.videoFrameExtractor.sourceWidth, self.videoFrameExtractor.sourceHeight);
    
    if(self.frameOperationQueue) {
        [self.frameOperationQueue cancelAllOperations];
        self.frameOperationQueue = nil;
        frameIndex = 0;
    }
    self.frameOperationQueue  = [[NSOperationQueue alloc] init];
    self.frameOperationQueue.maxConcurrentOperationCount  = 1;
	lastFrameTime = -1;
    
    // seek to 0.0 seconds
//	[self.videoFrameExtractor seekTime:0.0];
    
    // calculate total frames
//    self.totalNumberFrames = self.videoFrameExtractor.duration * FramePerSeconds;
    self.noMoreFrames = NO;
//    self.framesToCompare = 5;
    self.framesPosition = 0;
*/ 
}

- (void)startFrameExtractionFromIndex:(NSInteger)inIndex {
    /*
    [self configureFrameExtractor];
    self.frameCounter = inIndex;
    double timeToSeek = [self timeForFrame:inIndex];
    
//    NSLog(@"Seeking at %f seconds",timeToSeek);
    
//    [self.videoFrameExtractor seekTime:timeToSeek];
    [self.videoFrameExtractor seekAtFrame:inIndex];
    
    [self.delegate didStartExtractingFramesWithController:self];
    
	self.framingTimer =  [NSTimer scheduledTimerWithTimeInterval:1.0 / FramePerSeconds
									 target:self
								   selector:@selector(displayNextFrame:)
								   userInfo:nil
									repeats:YES];
    */ 

}
- (void)restartFraming {
#if OPTIMIZATION
    return;
#endif
    if(self.noMoreFrames) {
//        NSLog(@"No more frames available");
        return;
    }
    if(!self.framingTimer) {
        self.framesPosition = 0;
        [self.delegate didStartExtractingFramesWithController:self];
        NSLog(@"Framing restarted");
        self.frameIndex = 0;
        self.framingTimer =  [NSTimer scheduledTimerWithTimeInterval:1.0 / FramePerSeconds
                                                              target:self
                                                            selector:@selector(displayNextFrame:)
                                                            userInfo:nil
                                                             repeats:YES];
    } else {
//        NSLog(@"Framing in progress");
    }
}
- (void)stopFramingProcess {
    [self.framingTimer invalidate];
    self.framingTimer = nil;
}
- (void)configureFrame:(NSNumber *)inStartTime {
/*
    @autoreleasepool {
        UIImage *imageFrame = nil;
        if(self.landScapeVideo) {
            imageFrame = self.videoFrameExtractor.currentImage;
        } else {
            imageFrame = [self.videoFrameExtractor.currentImage rotate:UIImageOrientationRight];
        }
        [self performSelectorOnMainThread:@selector(addFrameForSerialize:) withObject:imageFrame waitUntilDone:YES];

        self.frameIndex++;
        self.frameCounter++;
    }
*/ 
}
- (void)addFrameForSerialize:(UIImage *)inImage {
    [self addFrame:inImage atIndex:self.frameCounter];
}
//-(void)displayNextFrame:(NSTimer *)timer {
//	NSTimeInterval startTime = [NSDate timeIntervalSinceReferenceDate];
//	if (self.totalNumberFrames <= self.frameCounter || self.frameIndex == self.framesToCompare) {
//        if(self.totalNumberFrames <= self.frameCounter) {
//            self.noMoreFrames = YES;
//        }
//		[timer invalidate];
//        self.framingTimer = nil;
//		return;
//	}
//    [self.videoFrameExtractor stepFrame];
//    [NSThread detachNewThreadSelector:@selector(configureFrame:) toTarget:self withObject:[NSNumber numberWithFloat:startTime]];
//}
-(void)displayNextFrame:(NSTimer *)timer {
/*
	NSTimeInterval startTime = [NSDate timeIntervalSinceReferenceDate];
	if (self.frameIndex >= self.framesToCompare) {
//        if(self.totalNumberFrames <= self.frameCounter) {
//            self.noMoreFrames = YES;
//        }
		[timer invalidate];
        self.framingTimer = nil;
		return;
	}
    [self.videoFrameExtractor stepFrame];
    [NSThread detachNewThreadSelector:@selector(configureFrame:) toTarget:self withObject:[NSNumber numberWithFloat:startTime]];
*/ 
}
- (void)addFrame:(UIImage *)inImage atIndex:(NSInteger)inIndex {
    FrameWriteOperation *frameOperation = [[FrameWriteOperation alloc] initWithImage:inImage withInddex:inIndex];
    frameOperation.folderName = self.folderName;
    frameOperation.delegate = self;
    frameOperation.videoID = 1;
    [self.frameOperationQueue addOperation:frameOperation];
}
- (void)operation:(FrameWriteOperation *)inOperation didFinishWritingAtIndex:(NSInteger)inIndex {
    [self performSelectorOnMainThread:@selector(updateFrameProgressWithFrameIndex:)
                           withObject:[NSNumber numberWithInteger:inIndex]
                        waitUntilDone:YES];
}
- (void)updateFrameProgressWithFrameIndex:(NSNumber *)inFrameIndex {
/*
    NSInteger frameNumber = [inFrameIndex integerValue];
#if OPTIMIZATION
    [self.delegate controller:self didGenerateFrame:frameNumber];
#else
    [self.delegate controller:self didExtractFrameAtIndex:frameNumber];
#endif
    self.framesPosition++;
    
    CGFloat percentage = (CGFloat)self.framesPosition / self.framesToCompare;
    [self.delegate controller:self didFinishPercent:percentage];
    if(self.framesPosition == self.framesToCompare || self.noMoreFrames) {
        self.currentDtsValue = self.videoFrameExtractor.dtsValue;
        [self.delegate didFinishExtractingFramesWithController:self];
    }
*/ 
}
- (NSInteger)framesToExtractForPercent:(CGFloat)inPercent {
    NSInteger framesToExtract = 0;
    NSInteger newFramesValue = ((self.totalNumberFrames * inPercent) / 100);
    if(newFramesValue > self.frameCounter) {
        framesToExtract = newFramesValue - self.frameCounter;
    }
    return framesToExtract;
}
- (void)dealloc {
    [self.frameOperationQueue cancelAllOperations];    
    [self.frameOperationQueue waitUntilAllOperationsAreFinished];
    self.frameOperationQueue = nil;
}
#pragma mark - Optimized approach to extract frames
- (void)extractFramesAtIndex:(NSNumber *)inIndex {
/*
    double frameTime = [self timeForFrame:[inIndex integerValue]];
    NSLog(@"Time : %f",frameTime);
    
	[self.videoFrameExtractor seekTime:frameTime];
    
    if(!self.framingTimer) {
        self.framesPosition = 0;
        self.framesToCompare = 5;
        [self.delegate didStartExtractingFramesWithController:self];
        NSLog(@"Framing restarted");
        self.frameIndex = 0;
        self.framingTimer =  [NSTimer scheduledTimerWithTimeInterval:1.0 / FramePerSeconds
                                                              target:self
                                                            selector:@selector(displayNextFrame:)
                                                            userInfo:nil
                                                             repeats:YES];
    }
*/ 
}
- (double)timeForFrame:(NSInteger)inFrameIndex {
    double duration = [self.delegate timeDurationSelectedVideo:self];
    double frameTime = ((((inFrameIndex * 100.0f) / (double)self.totalNumberFrames) * duration) / 100.0f);
    return frameTime;
}
- (void)frameAtIndex:(NSNumber *)inIndex {
/*
    @autoreleasepool {
        double frameTime = [self timeForFrame:[inIndex integerValue]];
        NSLog(@"Time : %f",frameTime);
        
        // Seek video to given time frame
        [self.videoFrameExtractor seekTime:frameTime];

        // Step frame and extract image out of it
        UIImage *imageFrame = nil;
        if([self.videoFrameExtractor stepFrame]) {
            if(self.landScapeVideo) {
                imageFrame = self.videoFrameExtractor.currentImage;
            } else {
                imageFrame = [self.videoFrameExtractor.currentImage rotate:UIImageOrientationRight];
            }
        }
        NSDictionary *frameInfo = [[NSDictionary alloc] initWithObjectsAndKeys:imageFrame,@"FRAME",
                                   [NSNumber numberWithInteger:[inIndex integerValue]],@"INDEX", nil];
        [self performSelectorOnMainThread:@selector(serializeFrame:) withObject:frameInfo waitUntilDone:YES];
    }
*/ 
}
- (void)serializeFrame:(NSDictionary *)inFrameInfo {
    UIImage * frame = [inFrameInfo objectForKey:@"FRAME"];
    NSNumber *index = [inFrameInfo objectForKey:@"INDEX"];
    [self addFrame:frame atIndex:[index integerValue]];
}
@end
