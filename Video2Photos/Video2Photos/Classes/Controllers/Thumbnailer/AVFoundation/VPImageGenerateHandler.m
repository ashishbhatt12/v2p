//
//  VPImageGenerateHandler.m
//  Video2Photos
//
//  Created by Ashish Bhatt on 25/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VPImageGenerateHandler.h"
#import <SDWebImage/SDImageCache.h>
//#import <MediaPlayer/MediaPlayer.h>
#define FPS 5.0f

// videoDuration / FPS = totalFrames 
// frameIndex * FPS = FrameTime

@interface VPImageGenerateHandler()
@property(nonatomic,strong)AVAsset *asset;
@property(nonatomic,strong)AVAssetImageGenerator *imageGenerator;
@property(nonatomic,strong)NSMutableArray* captureTimes;
@end
@implementation VPImageGenerateHandler
@synthesize imageGenerator;
@synthesize asset;
@synthesize delegate;
@synthesize totalNumberOfFrames;
@synthesize captureTimes;

@synthesize datasource = _datasource;
@synthesize frameIndex = _frameIndex;
@synthesize capturedIndex =_capturedIndex;
@synthesize landscapeVideo = _landscapeVideo;


#pragma mark - Latest optimized code
- (id)initWithAssetURL:(NSURL *)inURL {
    if(self = [super init]) {
        AVURLAsset *videoAsset = [[AVURLAsset alloc] initWithURL:inURL options:nil];
        self.imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:videoAsset];
        self.imageGenerator.requestedTimeToleranceAfter = kCMTimeZero;
        self.imageGenerator.requestedTimeToleranceBefore = kCMTimeZero;
//        self.imageGenerator.appliesPreferredTrackTransform = YES;
    }
    return self;
}
- (void)dealloc {
    self.delegate = nil;
    self.datasource = nil;
}

- (void)generateFramesAtIndex:(NSInteger)inIndex {
    // Stop generating frames if it is running
    [self.imageGenerator cancelAllCGImageGeneration];
    self.frameIndex = inIndex;
    self.capturedIndex = -1;
    
    [self initiateFramingFromIndex:inIndex];
}
- (NSArray *)prepareTimeRangesToCaptureThumbnails {
    
    NSInteger indexCounter = self.frameIndex;
    
    double startTime = [self.datasource secondsForFrameIndex:indexCounter forImageGenerator:self];

    CMTime totalDuration = [self.datasource videoDurationForImageGenerator:self];
    
    CMTime frameTime = CMTimeMakeWithSeconds(startTime, totalDuration.timescale);
    CMTime stepInterval = CMTimeMakeWithSeconds(1.0f / 24.0f, totalDuration.timescale);
    BOOL reachedAtEnd = NO;
    NSMutableArray *timeValues = [NSMutableArray arrayWithCapacity:10];
    
    NSInteger counter = 0;
    do {
        // First check if image is already cached or not
        UIImage *cachedImage = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:[NSString stringWithFormat:@"%d",indexCounter]];
        if(!cachedImage) {
            [timeValues addObject:[NSValue valueWithCMTime:frameTime]];
//            NSLog(@"Frame added to capture for index : %d",indexCounter);
            if(self.capturedIndex == -1 || self.capturedIndex > indexCounter) {
                self.capturedIndex = indexCounter;
            }
        }
        CMTime newTime = CMTimeAdd(frameTime, stepInterval);
        if(CMTimeCompare(newTime, totalDuration) == 1 || FRAMES_TO_CAPTURE <= counter) {
            // Video time reached at end of video or counter reached to maximum allowed frames
            reachedAtEnd = YES;
        } else {
            frameTime = newTime;
        }
        counter++;
        indexCounter++;
    } while (!reachedAtEnd);
    return timeValues;
}

- (void)initiateFramingFromIndex:(NSInteger)inIndex {
    // First prepare time values for next 10 frames
    NSArray * timeValues = [self prepareTimeRangesToCaptureThumbnails];

    __block NSInteger imageCounter = self.capturedIndex;

    [self.delegate didStartGeneratingImageSequence:self];
    
    // initiate generating frames asynchronously
    [self.imageGenerator generateCGImagesAsynchronouslyForTimes:timeValues
                                              completionHandler:^(CMTime requestedTime, CGImageRef image, CMTime actualTime,
                                                                  AVAssetImageGeneratorResult result, NSError *error) {
                                                  
                                                  NSString *requestedTimeString = (NSString *)CFBridgingRelease(CMTimeCopyDescription(NULL, requestedTime));
                                                  NSString *actualTimeString = (NSString *)CFBridgingRelease(CMTimeCopyDescription(NULL, actualTime));
                                                  NSLog(@"R : %@ || A : %@", requestedTimeString, actualTimeString);
                                                  
                                                  if (result == AVAssetImageGeneratorSucceeded) {
                                                      // Do something interesting with the image.
                                                      UIImage *currentImage = nil;
                                                      if(self.landscapeVideo) {
                                                          currentImage = [UIImage imageWithCGImage:image];
                                                      } else {
                                                          currentImage = [UIImage imageWithCGImage:image scale:1.0f orientation:UIImageOrientationRight];
                                                      }

                                                      NSDictionary *frameInfo = @{@"FRAME" : currentImage,@"INDEX" : [NSNumber numberWithInteger:imageCounter]};
                                                      [self performSelectorOnMainThread:@selector(updateFrameWithInfo:)
                                                                             withObject:frameInfo
                                                                          waitUntilDone:NO];

                                                      
                                                      if(imageCounter >= FRAMES_TO_CAPTURE + 10) {
                                                          [self.delegate performSelectorOnMainThread:@selector(didFinishGeneratingImageSequence:)
                                                                                          withObject:self
                                                                                       waitUntilDone:NO];
                                                      }
                                                    imageCounter++;
                                                  }
                                                  
                                                  if (result == AVAssetImageGeneratorFailed) {
                                                      NSLog(@"Failed with error: %@", [error localizedDescription]);
                                                  }
                                                  if (result == AVAssetImageGeneratorCancelled) {
                                                      NSLog(@"Canceled generating image");
                                                  }
                                              }];
}
- (void)updateFrameWithInfo:(NSDictionary *)inFrameInfo {
    UIImage *frame = inFrameInfo[@"FRAME"];
    NSNumber *index = inFrameInfo[@"INDEX"];
    [self.delegate handler:self didFindImage:frame forIndex:[index integerValue]];
    [[SDImageCache sharedImageCache] storeImage:frame forKey:[NSString stringWithFormat:@"%d",[index integerValue]]];
}

@end
