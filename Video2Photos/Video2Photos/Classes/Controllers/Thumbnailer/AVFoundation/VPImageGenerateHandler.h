//
//  VPImageGenerateHandler.h
//  Video2Photos
//
//  Created by Ashish Bhatt on 25/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
@protocol VPImageGeneratorDelegate;
@protocol VPImageGeneratorDatasource;
@interface VPImageGenerateHandler : NSObject {

}
@property(nonatomic,weak)NSObject<VPImageGeneratorDelegate> * __weak delegate;
@property(nonatomic,weak)NSObject<VPImageGeneratorDatasource> * __weak datasource;
@property(nonatomic,assign)NSInteger totalNumberOfFrames;
@property(nonatomic,assign)NSInteger frameIndex;
@property(nonatomic,assign)NSInteger capturedIndex;
@property(nonatomic,assign)BOOL landscapeVideo;

#pragma mark - Latest optimized code
- (id)initWithAssetURL:(NSURL *)inURL;
- (void)generateFramesAtIndex:(NSInteger)inIndex;
@end

@protocol VPImageGeneratorDelegate
@optional
- (void)handler:(VPImageGenerateHandler *)inHandler didFindImage:(UIImage *)inImage forIndex:(NSInteger)inIndex;
- (void)handler:(VPImageGenerateHandler *)inHandler didFindImage:(UIImage *)inImage;
- (void)didStartGeneratingImageSequence:(VPImageGenerateHandler *)inHandler;
- (void)didFinishGeneratingImageSequence:(VPImageGenerateHandler *)inHandler;
@end

@protocol VPImageGeneratorDatasource
- (double)secondsForFrameIndex:(NSInteger)inIndex forImageGenerator:(VPImageGenerateHandler *)inGenerator;
- (CMTime)videoDurationForImageGenerator:(VPImageGenerateHandler *)inGenerator;
@end