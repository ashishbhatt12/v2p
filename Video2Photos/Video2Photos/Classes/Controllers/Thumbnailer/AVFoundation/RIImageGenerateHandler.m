//
//  RIImageGenerateHandler.m
//  ReverseIt
//
//  Created by Ashish Bhatt on 8/23/12.
//  Copyright (c) 2012 Capgemini. All rights reserved.
//

#import "RIImageGenerateHandler.h"
#import "RIUtilities.h"

@interface RIImageGenerateHandler()
@property(nonatomic,strong)AVAsset *videoAsset;
@property(nonatomic,strong)AVAssetImageGenerator *imageGenerator;
@property(nonatomic,strong)NSMutableArray *captureTimes;
@end

@implementation RIImageGenerateHandler
@synthesize imageGenerator;
@synthesize captureTimes;
@synthesize videoAsset;
@synthesize delegate;
- (id)initWithVideoAsset:(AVAsset *)inVideoAsset {
    if(self = [super init]) {
        self.videoAsset = inVideoAsset;
    }
    return self;
}
- (void)prepareTimeRanges {
    @autoreleasepool {
        CMTime inDuration = self.videoAsset.duration;
        NSMutableArray *timeRanges = [[NSMutableArray alloc] init];
        CGFloat frameDuration = 0.3f;        
        NSInteger totalNumberOfFrames = CMTimeGetSeconds(inDuration) / frameDuration; 

        for (NSInteger index = 0; index < totalNumberOfFrames; index++) {
            CMTime startTime = CMTimeMakeWithSeconds(index * frameDuration, inDuration.timescale);
            CMTime duration = CMTimeMakeWithSeconds(frameDuration, inDuration.timescale);
            CMTime totalFrameTime = CMTimeAdd(startTime, duration);
            CMTimeRange frameRange = kCMTimeRangeZero;
            
            if(CMTimeCompare(totalFrameTime, inDuration) == 1) {
                CGFloat seconds = CMTimeGetSeconds(totalFrameTime) - CMTimeGetSeconds(inDuration);
                CMTime remainingTime = CMTimeMakeWithSeconds(seconds, inDuration.timescale);
                frameRange = CMTimeRangeMake(startTime,remainingTime);
                [timeRanges addObject:[NSValue valueWithCMTime:remainingTime]];                
                [self generateImageFromVideoForTime:remainingTime forImageIndex:index];
                
            } else {
                frameRange = CMTimeRangeMake(startTime, duration);
                [timeRanges addObject:[NSValue valueWithCMTime:startTime]];                
                [self generateImageFromVideoForTime:startTime forImageIndex:index];                
            }
        }
        self.captureTimes = timeRanges;
//        [self performSelectorOnMainThread:@selector(startGeneratingImages) withObject:nil waitUntilDone:NO];
    }
}
- (void)exportDidFinishScaling:(AVAssetExportSession*)session {
    if(session.status == AVAssetExportSessionStatusCompleted){
        //        [self.delegate handler:self didFinishMergingAtPath:[session.outputURL path]];
    }
}
- (void)applyScalingEffectOnVideo {
    AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
    //FIRST VIDEO TRACK
    AVMutableCompositionTrack *mutableTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    AVAssetTrack *assetTrack = [[self.videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    
    
    
    [mutableTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, self.videoAsset.duration) 
                          ofTrack:assetTrack
                           atTime:self.videoAsset.duration 
                            error:nil];
    
    
    
    [mutableTrack scaleTimeRange:CMTimeRangeMake(kCMTimeZero,CMTimeMakeWithSeconds(5.0f,self.videoAsset.duration.timescale)) 
                      toDuration:CMTimeMakeWithSeconds(2.0f, self.videoAsset.duration.timescale)];
    
     NSURL *outputURL = [NSURL fileURLWithPath:[[RIUtilities appDocumentsDirectory] stringByAppendingPathComponent:@"Scaled.mov"]];
    
    
    // Prepare final exporter for merging
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPreset640x480];
    exporter.outputURL = outputURL;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.shouldOptimizeForNetworkUse = YES;
    
    // Notify caller that merging process is about tro start
//    [self.delegate handler:self didStartMergingAtPath:[outputURL path]];
    
    [exporter exportAsynchronouslyWithCompletionHandler:^ {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self exportDidFinishScaling:exporter];
        });
    }];

}

- (void)generateImageFromVideoForTime:(CMTime)inTime forImageIndex:(NSInteger)inIndex{
    
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:self.videoAsset];
//    generate.maximumSize = CGSizeMake(640.0f, 480.0f);
    generate.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = inTime;
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    
    UIImage *currentImage = [[UIImage alloc] initWithCGImage:imgRef];
    
    CGImageRelease(imgRef);
    NSString *imageFilePath = [RIUtilities outputImageFramePathIndex:inIndex];
    // Write image to PNG
    [UIImagePNGRepresentation(currentImage) writeToFile:imageFilePath atomically:YES];
    
    
//    CGImageRelease(image);
    
}
- (void)startCaptureing {
//    [NSThread detachNewThreadSelector:@selector(prepareTimeRanges) toTarget:self withObject:nil];
//    [self prepareTimeRanges];
    [self applyScalingEffectOnVideo];
}
- (void)startGeneratingImages {
//    [self testGenerateThumbNailDataWithVideo];
//    return;
    __block NSInteger imageCounter = 0;
    self.imageGenerator = [AVAssetImageGenerator assetImageGeneratorWithAsset:self.videoAsset];
    self.imageGenerator.appliesPreferredTrackTransform = YES;
//    self.imageGenerator.maximumSize = CGSizeMake(320.0f, 480.0f); 
//    NSArray *captureTimes = [NSArray arrayWithObjects:[NSValue valueWithCMTime:CMTimeMakeWithSeconds(0, 30)], nil ];
    [self.imageGenerator generateCGImagesAsynchronouslyForTimes:self.captureTimes
                                         completionHandler:^(CMTime requestedTime, CGImageRef image, CMTime actualTime,
                                                             AVAssetImageGeneratorResult result, NSError *error) {
                                             
                                             NSString *requestedTimeString = (NSString *)CFBridgingRelease(CMTimeCopyDescription(NULL, requestedTime));
                                             NSString *actualTimeString = (NSString *)CFBridgingRelease(CMTimeCopyDescription(NULL, actualTime));
                                             NSLog(@"Requested: %@; actual %@", requestedTimeString, actualTimeString);
                                             
                                             if (result == AVAssetImageGeneratorSucceeded) {
                                                 // Do something interesting with the image.
                                                 UIImage *currentImage = [[UIImage alloc] initWithCGImage:image];
                                                 
                                                 NSString *imageFilePath = [RIUtilities outputImageFramePathIndex:imageCounter];
                                                 // Write image to PNG
                                                 [UIImagePNGRepresentation(currentImage) writeToFile:imageFilePath atomically:YES];

                                                 imageCounter++;
                                                 CGImageRelease(image);
                                             }
                                             
                                             if (result == AVAssetImageGeneratorFailed) {
                                                 NSLog(@"Failed with error: %@", [error localizedDescription]);
                                             }
                                             if (result == AVAssetImageGeneratorCancelled) {
                                                 NSLog(@"Canceled");
                                             }
                                         }];
}
// MARK: Methods for video list
- (void)grabFirstImageFrameForRowIndex:(NSInteger)inRowIndex withVideoIndex:(NSInteger)inVideoIndex {
    NSDictionary *paraDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                              [NSNumber numberWithInteger:inRowIndex],@"ROW",
                              [NSNumber numberWithInteger:inVideoIndex],@"VIDEO", nil];
    
    [NSThread detachNewThreadSelector:@selector(generateFirstFrame:) toTarget:self withObject:paraDict];
}
- (void)generateFirstFrame:(NSDictionary *)inParaDict {
    @autoreleasepool {

        NSNumber *rowIndex = [inParaDict objectForKey:@"ROW"];
        NSNumber *videoIndex = [inParaDict objectForKey:@"VIDEO"];        
        
        AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:self.videoAsset];
        generate.appliesPreferredTrackTransform = YES;
        CMTime time = CMTimeMakeWithSeconds(1.5f, self.videoAsset.duration.timescale);
        NSError *err = nil;
        CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
        UIImage *currentImage = [[UIImage alloc] initWithCGImage:imgRef];
        CGImageRelease(imgRef);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate handler:self didGenerateImage:currentImage forRowIndex:[rowIndex integerValue] withVideoIndex:[videoIndex integerValue]];
        });
             

    }
}

@end
