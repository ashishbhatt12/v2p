//
//  RIImageGenerateHandler.h
//  ReverseIt
//
//  Created by Ashish Bhatt on 8/23/12.
//  Copyright (c) 2012 Capgemini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
@protocol RIImageGenerateDelegate;
@interface RIImageGenerateHandler : NSObject  {
    @private
    NSObject<RIImageGenerateDelegate> * __weak delegate;
}
@property(nonatomic,weak)NSObject<RIImageGenerateDelegate> * __weak delegate;
- (id)initWithVideoAsset:(AVAsset *)inVideoAsset;
- (void)startCaptureing;
- (void)grabFirstImageFrameForRowIndex:(NSInteger)inRowIndex withVideoIndex:(NSInteger)inVideoIndex;
- (void)generateImageFromVideoForTime:(CMTime)inTime forImageIndex:(NSInteger)inIndex;
@end

@protocol RIImageGenerateDelegate
- (void)handler:(RIImageGenerateHandler *)inHandler didGenerateImage:(UIImage *)inImage 
    forRowIndex:(NSInteger)inIndex
 withVideoIndex:(NSInteger)inInVideoIndex;
@end
