//
//  VPFrameCaptureOperation.m
//  Video2Photos
//
//  Created by Ashish on 1/16/13.
//
//

#import "VPFrameCaptureOperation.h"
#import "RIUtilities.h"

@implementation VPFrameCaptureOperation
@synthesize delegate = _delegate;
@synthesize frameIndex = _frameIndex;
@synthesize frameTime = _frameTime;
@synthesize folderName = _folderName;

- (id)initWithFrameIndex:(NSInteger)inIndex forTime:(double)inTime forMovie:(NSURL *)inURL{
    if(self = [super init]) {
        self.frameIndex = inIndex;
        self.frameTime = inTime;
    }
    return self;
}
- (void)main {
    @autoreleasepool {
        if([self isCancelled])
            return;
        
        UIImage *thumbNail = [[self.delegate moviePlayerController] thumbnailImageAtTime:self.frameTime timeOption:MPMovieTimeOptionExact];
        
        if([self isCancelled])
            return;
        
        [self.delegate operation:self didCaptureFrame:thumbNail forIndex:self.frameIndex];
        
        /*
         if([self isCancelled])
         return;
         
         NSString *frameFilePath = [RIUtilities framePathAtIndex:self.frameIndex forFolder:self.folderName];
         
         if([[NSFileManager defaultManager] fileExistsAtPath:frameFilePath])
         return;
         
         if([self isCancelled])
         return;
         
         NSData * imageData = UIImagePNGRepresentation(thumbNail);
         
         if([self isCancelled])
         return;
         
         [imageData writeToFile:frameFilePath atomically:YES];
         */         
    }
}

- (void)dealloc {
    self.delegate = nil;
}
@end
