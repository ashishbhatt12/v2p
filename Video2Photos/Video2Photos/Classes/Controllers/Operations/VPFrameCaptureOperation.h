//
//  VPFrameCaptureOperation.h
//  Video2Photos
//
//  Created by Ashish on 1/16/13.
//
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

@protocol VPFrameCaptureDelegate;
@interface VPFrameCaptureOperation : NSOperation
@property(nonatomic,weak)NSObject<VPFrameCaptureDelegate> *__weak delegate;
@property(nonatomic,assign)NSInteger frameIndex;
@property(nonatomic,assign)double frameTime;
@property(nonatomic,strong)NSString *folderName;
- (id)initWithFrameIndex:(NSInteger)inIndex forTime:(double)inTime forMovie:(NSURL *)inURL;
@end

@protocol VPFrameCaptureDelegate
- (MPMoviePlayerController *)moviePlayerController;
- (void)operation:(VPFrameCaptureOperation *)inOperation didCaptureFrame:(UIImage *)inFrame forIndex:(NSInteger)inIndex;
@end