//
//  VPRootViewController_iphone.h
//  Video2Photos
//
//  Created by Ashish Bhatt on 20/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VPRootViewController.h"
#import "VPImageGenerateHandler.h"
#import "PosterImageView.h"
#import "iCarousel.h"
@interface VPRootViewController_iphone : VPRootViewController <iCarouselDataSource,iCarouselDelegate,
                                                        PosterImageViewSelectionDelegate,
                                    VPImageGeneratorDelegate,
                                    UIImagePickerControllerDelegate> 
{
    @private
    iCarousel * __weak carousel;
    UIActivityIndicatorView * __weak activityView;
}
@property (nonatomic, weak) IBOutlet iCarousel * __weak carousel;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView* __weak activityView;
@property(nonatomic,strong)VPImageGenerateHandler *imageGenerator;
- (VPImageGenerateHandler *)imageGeneratorHandler;
- (VPImageGenerateHandler *)imageGeneratorHandlerForAssetURL:(NSURL *)inURL;
- (IBAction)onCamera:(id)inSender;
@end
